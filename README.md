# 1. Overview

This is an implementation of a simple algorithm to predict the probability of web app users to discontinue their
subscriptions. The algorithm is based on statistical analysis of user behavior represented by an engagement score
which, in turn, is computed using a weighted set of features measuring the frequency of user interactions relevant to
the business process of the web app.

Users are grouped into a few categories based on their engagement levels. Analysis and prediction is done on each group.
Finally, group statistics are compared such as to verify or infirm the hypothesis that way users are distributed among
groups is representative for their behavior.

Statistical significance of distribution divergence is computed automatically, but the data is also visualized to help
verify the automatic hypotheses and to provide more insight to whoever tries to understand user behavior.

## 1.1. Showcased Expertise
- robust statistics: outlier detection for skewed univariate distributions
- interactive data visualization
- statistical significance tests
- design & implementation of probabilities & statistics algorithms

## 1.2. Tech Used

- [`JupyterLab`](https://jupyterlab.readthedocs.io)

### 1.2.1. Data Science
- [`NumPy`](https://numpy.org/)
- [`SciPy`](https://www.scipy.org/)

### 1.2.2 Data Visualization
- [`plotly`](https://plotly.com/)
- [`seaborn`](https://seaborn.pydata.org/)
- [`Matplotlib`](https://matplotlib.org/)
- check out [activity_analysis.html](activity_analysis.html) to see the plots we made

# 2. Setup

2.1. Make sure that you have a reasonably recent `Python` version installed and that the default python interpreter is
   `python3` (I've used `Python` 3.8 on my Ubuntu machine).

2.2. Create a `Python` virtual environment, activate it and install package dependencies. I've used
   [`virtualenv`](https://virtualenv.pypa.io/) from my IDE ([PyCharm](https://www.jetbrains.com/pycharm/)), but you
   should also use be able to use the builtin [`venv`](https://docs.python.org/3/library/venv.html) module (you don't
   need to install anything to use it, since it's packaged within recent versions of `Python`).
   [Here](https://docs.python.org/3/tutorial/venv.html) is a tutorial for `venv`. The commands you need to run should
   look something like this if you decide to use `virtualenv`:
- create a virtual environment:
```console
foo@bar:~/churn_prediction$ virtualenv venv
```
- activate the newly created environment (from `bash`):
```console
foo@bar:~/churn_prediction$ source venv/bin/activate
```
- install deps:
```console
(venv) foo@bar:~/churn_prediction$ pip install -r requirements.txt
```

2.3. For running the [`JupyterLab` notebook](activity_analysis.ipynb):
- install [`Node.js`](https://nodejs.org)
- create a virtual environment (not really required, you can just use the one created in `venv` at the previous step):
```console
foo@bar:~/churn_prediction$ virtualenv venv_jupyterlab
```
- activate the newly created environment (from `bash`):
```console
foo@bar:~/churn_prediction$ source venv_jupyterlab/bin/activate
```
- install deps:
```console
(venv_jupyterlab) foo@bar:~/churn_prediction$ pip install -r requirements_jupyterlab.txt
(venv_jupyterlab) foo@bar:~/churn_prediction$ jupyter nbextension enable --py widgetsnbextension --sys-prefix
(venv_jupyterlab) foo@bar:~/churn_prediction$ jupyter nbextension enable --py ipympl --sys-prefix
(venv_jupyterlab) foo@bar:~/churn_prediction$ jupyter labextension install jupyter-matplotlib jupyterlab-plotly plotlywidget @jupyter-widgets/jupyterlab-manager
```

# 3. Running the Code

Alternatives:

3.1. just use `Python` to execute:
- [activity_analysis.py](activity_analysis.py) to get descriptive statistics of user activity by engagement level
```console
(venv) foo@bar:~/churn_prediction$ python activity_analysis.py
```  
- [churn_prediction/churn_rate.py](churn_prediction/churn_rate.py) to compute the probability of users in each group to
churn over the next 30 days
```console
(venv) foo@bar:~/churn_prediction$ python -m churn_prediction.churn_rate
```
- [churn_prediction/speed_tests.py](churn_prediction/speed_tests.py) to run a benchmark for various methods to read from
csv  
```console
(venv) foo@bar:~/churn_prediction$ python -m churn_prediction.speed_tests.py
```

3.2. Use JetBrains' [`PyCharm`](https://www.jetbrains.com/pycharm/) IDE to open the directory (project files reside in
the [`.idea`](.idea) subdirectory) as a project. The project contains run configurations for each of the scripts above.

# 4. Acknowledgements & Further References

This code was written as part of a project for [InnerTrends](https://www.innertrends.com/), who were kind enough to
agree to share their data with the world. Check out the [project page](https://www.iddo.ro/portfolio/Statistics/) on Iddo
Software's website for further details about the project.
