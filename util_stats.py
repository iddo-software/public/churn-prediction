#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
helper functions that compute some statistics used in activity_analysis.py
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2018, InnerTrends & Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"


import math
import numpy

from churn_prediction.dataset import first_nonzero_idx, last_nonzero_idx


def fences(x, c=1.5, a=-4, b=3):
    from robustats import medcouple
    # matplotlib expects whisker ends in percentiles when drawing box plots
    mc = medcouple(x.astype(numpy.float32))
    if mc >= 0:
        f1, f2 = a, b
    else:
        f1, f2 = -b, -a

    lo_factor = c * math.exp(f1 * mc)
    hi_factor = c * math.exp(f2 * mc)

    q1 = numpy.percentile(x, 25)
    q3 = numpy.percentile(x, 75)
    iqr = q3 - q1

    lo = q1 - lo_factor * iqr
    hi = q3 + hi_factor * iqr

    samples = len(x)
    lo_outliers = numpy.count_nonzero(x < lo)
    hi_outliers = numpy.count_nonzero(x > hi)

    lo_quant = float(lo_outliers) / samples
    hi_quant = 1 - float(hi_outliers) / samples

    lo_perc = int(math.floor(lo_quant * 100))
    hi_perc = int(math.ceil(hi_quant * 100))

    return (lo, hi), (lo_perc, hi_perc)


def median(x, weights=None, x_sorted=False):
    # we implemented this function because boot.ci's documentation says that
    # it can use a statsfunction that supports weights. However, upon inspecting
    # the code, we determined that is a lie, and weights are used only when
    # method=='bca'. Actually, passing the data as (values, weights) tuple would
    # also work, but I'm not sure that the sampling would be correct.
    if weights is None:
        return numpy.median(x)

    x = numpy.asarray(x)
    weights = numpy.asarray(weights)

    assert x.ndim == 1
    assert weights.ndim == 1
    assert len(x) != 0
    assert len(x) == len(weights)
    assert numpy.all(weights >= 0)
    assert numpy.any(weights > 0)

    if not x_sorted and numpy.any(numpy.diff(x) < 0):
        sorter = numpy.argsort(x)
        x = x[sorter]
        weights = weights[sorter]

    cum_hist = numpy.cumsum(weights)
    half_total_weight = cum_hist[-1] / 2.

    idx_med = numpy.searchsorted(cum_hist, half_total_weight)

    if cum_hist[idx_med] == half_total_weight:
        # check is useless because cum_hist[-1] == half_total_weight * 2
        # assert idx_med < len(cum_hist) - 1
        return (x[idx_med] + x[idx_med + 1]) / 2
    return x[idx_med]


def median_conf_intervals(x, counts=None, rng=None, boots=10000, alpha=.05, method='lun'):
    # initial version (works, but not does not handle skewed medians well)
    # return boot.ci(x, numpy.median, method='pi')
    from tqdm import trange

    if numpy.iterable(alpha):
        alphas = numpy.array(alpha)
    else:
        alphas = numpy.array([alpha / 2., 1 - alpha / 2.])

    assert alphas.ndim == 1
    assert len(alphas) == 2
    assert numpy.all(alphas >= 0)
    assert numpy.all(alphas <= 1)

    x = numpy.asarray(x)
    assert x.ndim == 1
    assert len(x) != 0

    try:
        # rng can be a seed (int or array of ints)
        rng = numpy.random.RandomState(rng)
    except TypeError:
        pass

    if counts is None:
        count = len(x)
        weights = None
    else:
        assert numpy.all(counts > 0)
        count = numpy.sum(counts)
        assert count > 0
        weights = numpy.asarray(counts, numpy.float64) / count
        assert weights.ndim == 1
        assert len(weights) == len(x)

    boot_meds = numpy.empty(boots, x.dtype)
    for boot_id in trange(boots):
        # would be nice if we had an equivalent of rng.choice that also
        # returned the data as an array with counts; this way we would be
        # able to use our implementation of the median with weights, above
        if weights is None:
            # seems faster than the one below
            boot_meds[boot_id] = numpy.median(x[rng.randint(len(x), size=len(x))])
        else:
            boot_meds[boot_id] = numpy.median(rng.choice(x, count, p=weights))

    boot_meds.sort()

    alpha_idxs = numpy.round((boots - 1) * alphas).astype(int)
    lo, hi = boot_meds[alpha_idxs]

    if method == 'pi':
        # percentile method
        return lo, hi
    elif method == 'lun':
        # what Howell calls "Lunneborg's Method"
        # https://www.uvm.edu/~dhowell/StatPages/Randomization%20Tests/BootstMedians/bootstrapping_medians.html
        med = median(x, counts)
        # return med - b, med + a
        a = med - lo
        b = hi - med

        assert a >= 0
        assert b >= 0

        return med - b, med + a

    elif method == 't':
        # this one is more accurate, but also takes a lot longer, because it needs to bootstrap
        # again for at least 50 times for each bootstrapped sample
        raise NotImplementedError("TODO: implement Howell's 'Bootstrapped t intervals' method")

    else:
        raise ValueError('unknown method ' + str(method))


def min_range_hist(vals, counts, weight):
    assert weight >= 0
    assert weight <= 1

    counts = numpy.asarray(counts)
    assert counts.ndim == 1
    assert numpy.issubdtype(counts.dtype, numpy.integer)
    assert numpy.all(counts >= 0)
    assert numpy.any(counts)

    vals = numpy.asarray(vals)
    assert vals.ndim == 1
    assert numpy.all(numpy.diff(vals) > 0)  # check sorted, unique

    assert len(vals) == len(counts)

    if len(vals) == 0:
        return vals.copy()  # return empty array of the same type

    if weight == 0:
        return vals[0], vals[0]

    if weight == 1:
        return vals[first_nonzero_idx(counts)], vals[last_nonzero_idx(counts)]

    cum_hist = numpy.cumsum(counts)

    # print('cum hist:', cum_hist)

    weight = int(math.ceil(weight * cum_hist[-1]))
    # same thing, just doesn't crash on empty input
    # weight = int(math.ceil(weight * numpy.sum(counts)))

    # print('weight:', weight)

    # print('end:', end)

    # print('start:', start)

    min_range = None

    last_end = len(vals) - 1
    # first index which accumulates at least 'weight' samples; always <= last_end
    # because weight <= 1
    end = numpy.searchsorted(cum_hist, weight)
    while end <= last_end:
        # if we add counts[end] samples, we can discard the same number of low samples
        # start = numpy.searchsorted(cum_hist[:end], counts[end], side='right')
        # actually, we might be able to discard more than counts[end] samples:
        # if counts[end] is large enough it doesn't require all previous values
        start = numpy.searchsorted(cum_hist[:end], cum_hist[end] - weight, side='right')
        if not min_range or vals[end] - vals[start] < min_range[1] - min_range[0]:
            min_range = vals[start], vals[end]

        vals = vals[start:]
        counts = counts[start:]
        if start != 0:
            cum_hist -= cum_hist[start - 1]
        cum_hist = cum_hist[start:]

        last_end -= start
        end -= start - 1  # -1 increments end
    return min_range


def custom_bxpstats(x, label, min_range_weight=.95, med_ci=False):
    stats = dict()
    stats['label'] = label + ' Engagement'
    (lo_fence, hi_fence), f_perc = fences(x)
    stats['whislo'] = max(lo_fence, x.min())
    stats['whishi'] = min(hi_fence, x.max())

    inlier_mask = (x >= lo_fence) & (x <= hi_fence)
    inliers = x[inlier_mask]

    stats['fliers'] = x[~inlier_mask]
    stats['mean'] = numpy.mean(inliers)
    stats['med'] = numpy.median(inliers)

    inlier_counts = numpy.bincount(inliers)
    inlier_vals = numpy.arange(len(inlier_counts))

    if min_range_weight is None:
        # standard first and third quartiles
        stats['q1'], stats['q3'] = numpy.percentile(x, (25, 75))
    else:
        stats['q1'], stats['q3'] = min_range_hist(inlier_vals, inlier_counts, min_range_weight)

    if med_ci:
        stats['cilo'], stats['cihi'] = median_conf_intervals(inliers)

    return stats


def vars_independent(contingency_table, confidence=.95, gtest_ratio=.2):
    """
    param gtest_ratio: default 20% (as per pearson's chi squared test wiki page)
    """
    from scipy.stats import chi2_contingency
    from scipy.stats.contingency import expected_freq

    contingency_table = numpy.asarray(contingency_table)
    assert contingency_table.ndim == 2
    assert contingency_table.shape[0] > 1
    assert contingency_table.shape[1] > 1

    # eliminate all-0 rows and columns (e.g., histograms that are padded too much etc.)
    # @TODO: think: should we use sum instead of any?
    nonzero_mask = ~numpy.isclose(contingency_table, 0)
    col_mask = numpy.any(nonzero_mask, axis=0)
    row_mask = numpy.any(nonzero_mask, axis=1)

    contingency_table = contingency_table[row_mask][:, col_mask]

    samples = contingency_table.sum()
    if samples < 1000:
        if contingency_table.size == 4:
            # 2x2 contingency table
            raise NotImplementedError("Boschloo's test")
        else:
            raise NotImplementedError("Fisher's exact test for contingency tables > 2x2")
    else:
        expected = expected_freq(contingency_table)
        pearson_n_minus_1 = False
        lambda_ = None
        if contingency_table.size == 4:
            # 2x2 contingency table
            if numpy.any(expected) < 1:
                raise NotImplementedError("Boschloo's test")
            else:
                pearson_n_minus_1 = True
        elif (numpy.any(expected < 1)
              or numpy.count_nonzero(expected < 5) > gtest_ratio * contingency_table.size
              or numpy.any(contingency_table > 2 * expected)):
            lambda_ = 'log-likelihood'  # G-Test

        # @TODO: repeat test for expected values of 0 for G-Test

        chi2, p, dof, expected2 = chi2_contingency(contingency_table, correction=False, lambda_=lambda_)
        if pearson_n_minus_1:
            raise NotImplementedError("'N - 1' version of Pearson's Chi Squared Test")

        return p < 1. - confidence


def hists_independent(hist1, hist2, confidence=.95):
    hist1 = numpy.asarray(hist1)
    hist2 = numpy.asarray(hist2)

    assert hist1.ndim == 1
    assert hist2.ndim == 1

    assert len(hist1) == len(hist2)

    contingency_table = numpy.vstack((hist1, hist2)).T

    return vars_independent(contingency_table, confidence)


def get_level_activity_limits(activity_hist, level_activity_hists, lowest_activity):
    level_activity_limits = []
    for idx in range(len(level_activity_hists)):
        at_least_hist = level_activity_hists[idx:].sum(axis=0)
        lower_hist = activity_hist - at_least_hist
        # at_least_min = the activity frequency from which it's more likely that a user has at least the current engagement level
        #              = the index starting from which all at_least_hist > lower_hist
        # lower_sup = the last index before at_least_min
        lower_sup = last_nonzero_idx(at_least_hist <= lower_hist)
        if lower_sup is None:
            # at_least_hist is always > lower_hist
            # at_least_min = lowest_freq
            at_least_min = None
        # elif lower_sup == 0:
        #    # part of default case
        #    pass
        elif lower_sup == len(at_least_hist) - 1:
            # lower_hist is always >= at_least_hist
            at_least_min = None
        else:
            at_least_min = lower_sup + 1

        at_most_hist = lower_hist + level_activity_hists[idx]
        upper_hist = at_least_hist - level_activity_hists[idx]
        # at_most_max = the activity frequency upto which it's more likely that a user has at most the current engagement level
        #             = the index upto which at_most_hist is always > upper_hist
        # upper_inf = the first index after at_most_max
        upper_inf = first_nonzero_idx(upper_hist >= at_most_hist)
        if upper_inf is None:
            # at_most_hist is always > upper_hist
            # at_most_max = highest_freq
            at_most_max = None
        elif upper_inf == 0:
            # upper_hist is always >= at_most_hist
            at_most_max = None
        # elif upper_inf == len(at_most_hist) - 1:
        #     # part of default case
        #     pass
        else:
            at_most_max = upper_inf - 1

        if at_least_min is not None:
            at_least_min += lowest_activity
        if at_most_max is not None:
            at_most_max += lowest_activity

        level_activity_limits.append((at_least_min, at_most_max))

    return level_activity_limits


#########
# TESTS #
#########


def test_min_range_hist():
    # degenerate cases
    assert min_range_hist([1, 2, 3, 4, 5], [1, 1, 1, 1, 1], 0) == (1, 1)
    assert min_range_hist([1, 2, 3, 4, 5], [1, 1, 1, 1, 1], 1) == (1, 5)
    assert min_range_hist([1, 2, 3, 4, 5], [0, 1, 1, 1, 1], 1) == (2, 5)
    assert min_range_hist([1, 2, 3, 4, 5], [1, 1, 1, 1, 0], 1) == (1, 4)
    assert min_range_hist([1, 2, 3, 4, 5], [0, 1, 1, 1, 0], 1) == (2, 4)

    # trivial cases
    assert min_range_hist([100], [9], .5) == (100, 100)

    # simple cases
    assert min_range_hist([1, 2, 3, 4, 5], [1, 1, 1, 1, 1], .5) == (1, 3)
    assert min_range_hist([1, 2, 3, 4, 5], [1, 1, 1, 1, 1], .8) == (1, 4)
    assert min_range_hist([1, 2, 3, 4, 5], [1, 1, 1, 1, 1], .1) == (1, 1)

    # normal cases
    assert min_range_hist([10, 20, 30, 31, 32], [1, 1, 1, 1, 1], .5) == (30, 32)
    assert min_range_hist([1, 2, 3, 13, 23], [1, 1, 1, 1, 1], .5) == (1, 3)
    assert min_range_hist([10, 20, 30, 31, 32, 33, 43, 53], [1, 1, 1, 1, 1, 1, 1, 1], .5) == (30, 33)
    assert min_range_hist([1, 2, 3, 4, 5], [1, 1, 1, 2, 2], .5) == (4, 5)
    assert min_range_hist([1, 2, 3, 4, 5, 6], [1, 1, 0, 1, 1, 1], .5) == (4, 6)

    # boundary cases

    # single value has > 'weight' samples, that value can be first/last
    # odd number of samples
    assert min_range_hist([10, 20, 30, 40, 50], [1, 1, 1, 1, 5], .5) == (50, 50)
    assert min_range_hist([10, 20, 30, 40, 50], [5, 1, 1, 1, 1], .5) == (10, 10)
    assert min_range_hist([10, 20, 30, 40, 50], [1, 1, 5, 1, 1], .5) == (30, 30)
    # even number of samples
    assert min_range_hist([10, 20, 30, 40], [1, 1, 1, 3], .5) == (40, 40)
    assert min_range_hist([10, 20, 30, 40], [3, 1, 1, 1], .5) == (10, 10)
    assert min_range_hist([10, 20, 30, 40], [1, 3, 1, 1], .5) == (20, 20)


def run_tests():
    test_min_range_hist()


def main():
    run_tests()


if __name__ == '__main__':
    main()
