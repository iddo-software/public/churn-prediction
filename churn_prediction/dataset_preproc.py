#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
convert the data from sequential format to the matrix format we use for churn estimation
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2018, InnerTrends & Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

import numpy


def engagement_scores(engagement_points, window_size=numpy.timedelta64(30, 'D'), ret_points_matrix=False):
    """
    Computes the matrix of engagement scores from the list of timestamped engagement points.
    The engagement score for a particular user at a particular timestamp is the product between the sum of the
    engagement points of the user in the <window_size>-sized time span before that timestamp and the scoring frequency
    over that same time span.
    Scoring frequency is defined by the number of times the user has scored points during a time span, divided by the
    number of time units in the time span.
    The time unit used is that of the window_size. Timestamps in engagement_points are converted to this unit, and the
    resulting scores matrix is indexed by increments of this unit on its second axis (time). The numpy type system
    allows for choosing arbitrarily-sized time units. E.g., one can choose to take into account, when computing the
    score, a 30 day long window using 4 hour long time units by passing window_size=numpy.timedelta64(180, ('h', 4))
    :param engagement_points: a numpy array of (points, timestamp, num_uid) records
    :param window_size: a numpy.timedelta64 object that defines the length of the time span to use for computing scores.
                        A typical choice would be the churn time. Longer windows will result in having scores for users
                        that have churned.
    :param n_users: the number of unique elements in the uid column; if None, it is extracted automatically from
                    engagement_points (optional, gives a rather insignificant performance boost)
    :return: scores, earliest_timestamp:
             - scores is a matrix of shape (n_users, poi_length), poi_length being the number of time units between
               (and including) the earliest and latest timestamp in the engagement_points table
             - earliest_timestamp is the minimum timestamp found in the engagement_points table, converted to the time
               unit of window_size; useful for conveying information to the client code regarding how scores is indexed
    """

    # previously, we would assume that engagement_points was sorted by timestamp
    # check that engagement_points is sorted by timestamp
    # assert numpy.all(engagement_points['timestamp'][1:] >= engagement_points['timestamp'][:-1])

    uids = set(engagement_points['uid'])
    n_users = len(uids)
    assert n_users == max(uids) + 1

    # all time data is to be expressed in terms of the units of window_size
    time_unit = numpy.datetime_data(window_size)
    window_size = numpy.int64(window_size)

    # use a dummy datetime object to get the dtype of the window for mass conversion (we can't just use the dtype of
    # window_size because it's a timedelta and we need a datetime)
    time_dtype = numpy.datetime64('2018', time_unit).dtype

    # convert the timestamps to the time unit of the window, if necessary
    timestamps = numpy.asarray(engagement_points['timestamp'], time_dtype)

    earliest_timestamp = timestamps.min()

    # poi = Period of Interest
    poi_length = numpy.int64(timestamps.max() - earliest_timestamp) + 1

    # allocate output matrix (float64)
    points = numpy.zeros((n_users, poi_length))

    # slower equivalent of implementation below using numpy's advanced indexing
    # for points, timestamp, uid in engagement_points:
    #     time_idx = numpy.int64(timestamp - earliest_timestamp)
    #     scores[uid, time_idx] = points

    timestamp_index = (timestamps - earliest_timestamp).astype(numpy.int64)
    points[engagement_points['uid'], timestamp_index] = engagement_points['points']

    # in how many of the previous 30 days points were scored
    window_count = numpy.cumsum(points != 0, axis=1)
    window_count[:, window_size:] -= window_count[:, :-window_size]

    # running sum over a window of 30 days for scores
    scores = numpy.cumsum(points, axis=1)
    scores[:, window_size:] -= scores[:, :-window_size]

    # two lines to avoid allocating a temp array
    scores *= window_count
    scores *= 1. / window_size  # mul is faster than div

    # code to keep the earliest_timestamp in the original time unit to avoid data loss; we disable this behavior to
    # allow clients to infer stuff about the timediffs between columns in scores from the timedata of the returned
    # earliest_timestamp
    # return scores, engagement_points['timestamp'].min()

    if ret_points_matrix:
        return scores, earliest_timestamp, points
    else:
        return scores, earliest_timestamp


def get_masks(active_periods, activity_end_time, poi_start, poi_end, churn_time, prediction_time=None,
              print_extra_stats=False):
    """
    converts activity lists to bool masks to be used with engagement scores for churn rate estimation
    :param active_periods: a dict of numpy arrays: {user_id_0: [[user0_period0_start, user0_period0_end],
                                                          [user0_period1_start, user0_period1_end],
                                                          ...
                                                         ]
                                             user_id_1: [[user1_period0_start, user1_period0_end],
                                                         [user1_period1_start, user1_period1_end],
                                                          ...
                                                        ]
                                             }
                     the periods are assumed not to overlap and to be sorted in ascending order
    :param poi_start: timestamp before which any timestamp is considered to be outside of the period of interest
    :param poi_end: timestamp after which any timestamp is considered to be outside of the period of interest
    :param churn_time: timedelta - how long a user has to be inactive to be considered that he churned
    :param prediction_time: mark this many days before a churn as True in will_churn
    :param activity_end_time: timestamp after which we don't have any more data regarding user activity
    :return: is_active, will_churn, and unpredictive where:
             - is_active indicates the timestamps where the user was active
             - will_churn indicates the timestamps where the user was active, and the first churn time is less than
               prediction_time away
             - unpredictive indicates the days in which the user was active, and the first activity period end time is
               less than <prediction_time> away, but it's uncertain whether the activity period's end time is a churn or
               not because it was too close to activity_end_time to tell, so those samples should be excluded from
               predicting either churns nor non-churns
    """

    # make sure the same units are used
    assert numpy.datetime_data(poi_start) == numpy.datetime_data(poi_end)
    assert activity_end_time >= poi_end

    # compute how many timestamps are in the matrix of scores / activity / will_churn
    poi_size = numpy.int64(poi_end - poi_start) + 1

    # allocations
    is_active = numpy.zeros((len(active_periods), poi_size), bool)

    if prediction_time is not None:
        # more allocations
        will_churn = numpy.zeros_like(is_active)
        unpredictive = numpy.zeros_like(is_active)
        # convert the time unit of prediction_time to the one used to index activities and engagement scores
        prediction_time = numpy.timedelta64(prediction_time, numpy.datetime_data(poi_start))

        # precompute how many timestamps before a churn to consider an ominous for that churn
        prediction_idx_diff = numpy.int64(prediction_time)

    if print_extra_stats:
        n_active_periods = numpy.empty(len(active_periods), numpy.uint64)
        n_churns = numpy.empty(len(is_active), numpy.uint64)
        churns_per_active_periods = numpy.zeros((5, 5), numpy.uint64)
        n_churned_users = 0

    for uid, user_active_periods in enumerate(active_periods):
        # we'll treat the last period separately later
        periods_idx = (user_active_periods[:-1] - poi_start).astype(numpy.int64)
        if len(periods_idx):
            periods_idx[:, 1] += 1  # numpy slice semantics require conversion from "end" to "stop"
            periods_idx[0, 0] = max(periods_idx[0, 0], 0)  # first activity period might begin before poi if not trimmed

            for active_period_start_idx, active_period_stop_idx in periods_idx:
                is_active[uid, active_period_start_idx:active_period_stop_idx] = True

            if prediction_time is not None:
                # reuse periods_idx array in place for starts and stops for will_churn; note that we only mark an
                # element in will_churn as True if it is also marked as True in is_active
                periods_idx[:, 0] = numpy.maximum(periods_idx[:, 0], periods_idx[:, 1] - prediction_idx_diff)

                for will_churn_start_idx, will_churn_stop_idx in periods_idx:
                    will_churn[uid, will_churn_start_idx: will_churn_stop_idx] = True

        last_active_start, last_active_end = user_active_periods[-1]
        last_active_start_idx = numpy.int64(last_active_start - poi_start)

        # if the user has a single period, then the last is also the first, which might start before poi_start
        last_active_start_idx = max(last_active_start_idx, 0)
        last_active_end_idx = numpy.int64(last_active_end - poi_start)
        last_active_stop_idx = last_active_end_idx + 1

        # sanity check: activity periods are supposed to overlap with the poi
        assert last_active_stop_idx > 0

        is_active[uid, last_active_start_idx: min(last_active_stop_idx, poi_size)] = True

        # TODO: we assume that only the last active period can end after activity_end_time - churn_time; this is true
        #       (for now) because we drop periods starting after poi_end when loading activity data, and because of the
        #       assertion that activity_end_time >= poi_end at the beginning of the function
        if prediction_time is not None:
            # only select churn-ominous days in which the user was also active
            last_will_churn_start_idx = max(last_active_start_idx, last_active_stop_idx - prediction_idx_diff)

            if activity_end_time is None or last_active_end <= activity_end_time - churn_time:
                # we only mark if we know for sure that a churn happened
                mask_to_update = will_churn
            else:
                # we don't know whether a churn happened all not, so we just don't take these samples into account
                mask_to_update = unpredictive

            # the churn-ominous period might start or stop after the poi
            mask_to_update[uid, min(last_will_churn_start_idx, poi_size): min(last_active_stop_idx, poi_size)] = True

        if print_extra_stats:
            n_user_active_periods = len(user_active_periods)
            n_user_churns = len(periods_idx)
            if activity_end_time is None or last_active_end <= activity_end_time - churn_time:
                n_user_churns += 1
                n_churned_users += 1
            n_active_periods[uid] = n_user_active_periods
            n_churns[uid] = n_user_churns
            churns_per_active_periods[n_user_active_periods][n_user_churns] += 1

    if print_extra_stats:
        n_users = len(active_periods)
        print('overall churn rate: {:.2%} ({:,} / {:,})'.format(float(n_churned_users) / n_users, n_churned_users,
                                                                n_users))
        print('histogram of per-user active periods:')
        print(numpy.bincount(n_active_periods.astype(numpy.int64)))
        print('histogram of per-user churns:')
        print(numpy.bincount(n_churns.astype(numpy.int64)))
        print('histogram of per-user active_periods vs churns:')
        print(churns_per_active_periods)

    if prediction_time is None:
        return is_active

    return is_active, will_churn, unpredictive


def get_activity_mask(activity_timestamps, poi_start, poi_end):
    assert numpy.datetime_data(poi_start) == numpy.datetime_data(poi_end)

    activity_mask = numpy.zeros((len(activity_timestamps), numpy.int64(poi_end - poi_start + 1)), numpy.bool)

    for uid, timestamps in enumerate(activity_timestamps):
        if timestamps.dtype != poi_start.dtype:
            # if timestamps have been loaded with load_activity, they already should be in the same format as poi limits
            timestamps = numpy.unique(timestamps.astype(poi_start.dtype))

        timestamp_indices = (timestamps - poi_start).astype(numpy.int64)
        activity_mask[uid, timestamp_indices] = True

    return activity_mask


def get_activity_count(activity_mask, window_size):
    freq = numpy.cumsum(activity_mask, axis=1)
    freq[:, window_size:] -= freq[:, :-window_size]
    return freq
