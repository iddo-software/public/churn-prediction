#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
stuff that we wrote but gave up on using, in the end
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2018, InnerTrends & Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

from sortedcontainers import SortedSet
import numpy
import csv
import gc


def trim_activity(timestamps, timestamp_start, timestamp_stop, churn_time):
    # remove timestamps outside of period of interest

    timestamps = SortedSet(timestamps)

    # index of start date
    idx_start = timestamps.bisect_left(timestamp_start)
    if idx_start == len(timestamps):
        # all activity happened before the period of interest, so we ignore this user; we can actually also skip
        # the empty timestamps list check before the bisect (above), since this check would include that one
        # (we won't, though, since we also avoid doing the bisect in case the list is empty; a bisect on an
        # empty list is probably insignificant, so this decision is arbitrary)
        return None

    if timestamps[idx_start] == timestamp_start:
        # the start date was found in the activity log
        pass
    elif idx_start == 0:
        # all activity happens after the start of the period of interest
        pass
    else:
        # start of period of interest is between two activity entries
        if timestamps[idx_start] - timestamps[idx_start - 1] <= churn_time:
            # if user was active (i.e., not churned) at the start of the period of interest, then his first
            # engagement period should start before the period of interest, and include its start
            idx_start -= 1

    idx_stop = timestamps.bisect_right(timestamp_stop)
    if idx_stop == 0:
        # all activity happens after the stop of the period of interest(or the list is empty), so we skip this
        # user altogether
        return None

    if idx_stop == len(timestamps):
        # all activity has happened before the end of the period of interest
        pass
    elif timestamps[idx_stop - 1] == timestamp_stop:
        # the stop date was found in the activity log
        pass
    else:
        # end date of period of interest is between two activity entries
        if timestamps[idx_stop] - timestamps[idx_stop - 1] <= churn_time:
            # if user was active (i.e., not churned) at the end of the period of interest, then his last
            # engagement period should end after the period of interest, and include its end timestamp
            idx_stop += 1

    # convert to numpy array for lower memory and fast contiguous access
    timestamps = numpy.array(timestamps[idx_start: idx_stop])

    return timestamps


def trim_activity(activity, poi_start, poi_stop):
    # activity is expected to be a numpy array like [[period0_start, period0_stop], [period1_start, period1_stop], ...]

    # drop periods that end before poi_start; in order to do that, we look for the first period that ends
    # after (or on) poi_start
    if numpy.isnat(activity[-1, 1]):
        # the end timestamp of the last period might be 'nat', because the user's last activity timestamp is too close
        # to the end of the period in which we have activity logs for us to tell whether the user has churned or not
        idx_start = numpy.searchsorted(activity[:-1, 1], poi_start)

        # if there are any activity periods that end before the poi_start, idx_start will indicate the first period
        # that doesn't do that, i.e., the first valid period; if all the periods, except for the last one are invalid,
        # idx_start will be len(activity) - 1, i.e., the index of the period we don't know when it ends; however, since
        # the period ends at least at query_stop_date, which we know is after poi_stop, and, implicitly,
        # after poi_start, the last period is considered to be valid
    else:
        idx_start = numpy.searchsorted(activity[:, 1], poi_start)
    activity = activity[idx_start:]

    # search for the first period that starts after poi_stop
    idx_stop = numpy.searchsorted(activity[:, 0], poi_stop, side='right')
    return activity[:idx_stop]


def churns_from_activity(timestamps_activity, query_stop_date, churn_time):
    """

    :param timestamps_activity: sorted array, no duplicates
    :param query_stop_date: time after which we don't know whether the user was active or not
    :param churn_time: after being inactive for this long, a user is considered to be churned
    :return: timestamps of the last activity log entries after which the user has churned
    """

    # turn to numpy array for faster contiguous access
    timestamps_activity = numpy.asarray(timestamps_activity)

    churn_timestamps = []
    for timestamp, next_timestamp in zip(timestamps_activity[:-1], timestamps_activity[1:]):
        if next_timestamp - timestamp > churn_time:
            churn_timestamps.append(timestamp)

    if query_stop_date - timestamps_activity[-1] > churn_time:
        churn_timestamps.append(timestamps_activity[-1])

    return churn_timestamps


def get_activity_periods(timestamps, churn_time):
    # for each user we make a list of (start, stop) for each active period (i.e., the dates between which the
    # lapse between two activity log entries has not been longer than the churn time)
    timestamps = numpy.array(timestamps)

    # first implementation

    activity_periods = [[timestamps[0], timestamps[0]]]

    for timestamp in timestamps[1:]:
        if timestamp - activity_periods[-1][-1] <= churn_time:
            activity_periods[-1][-1] = timestamp
        else:
            activity_periods.append([timestamp, timestamp])
    return activity_periods


def get_activity_periods_vectorized(timestamps, churn_time):
    # for each user we make a list of (start, stop) for each active period (i.e., the dates between which the
    # lapse between two activity log entries has not been longer than the churn time)
    timestamps = numpy.array(timestamps)

    # first implementation

    activity_periods = [[timestamps[0], timestamps[0]]]

    for timestamp in timestamps[1:]:
        if timestamp - activity_periods[-1][-1] <= churn_time:
            activity_periods[-1][-1] = timestamp
        else:
            activity_periods.append([timestamp, timestamp])
    return activity_periods

    # vectorized - not finished, does not work:

    # timestamp_diffs = timestamps[1:] - timestamps[:-1]
    # churn_mask = timestamp_diffs > churn_time
    # churn_timestamp_idxs = churn_mask.nonzero()  # does not include last index
    # period_stop_idxs = period_start_idxs + 1
    #
    # period_starts = timestamps[period_start_idxs]
    # period_stops = timestamps[period_stop_idxs]
    #
    # periods = numpy.vstack((period_starts, period_stops)).T

    # vectorized one liner - not finished, does not work:

    # start_idxs = numpy.nonzero(timestamps[1:] - timestamps[:-1] > churn_time)
    # periods = numpy.vstack((timestamps[start_idxs], timestamps[start_idxs + 1])).T
    #
    # return periods


def first_true(bool_arr, axis, invalid_val=-1):
    # https://stackoverflow.com/questions/47269390/
    return numpy.where(bool_arr.any(axis=axis), bool_arr.argmax(axis=axis), invalid_val)


def trim_timestamps(timestamps, poi_start=None, poi_end=None):
    # keeps all timestamps >= poi_start up to (and including, if it exists) the first timestamp after poi_end
    if poi_start is not None:
        idx_start = numpy.searchsorted(timestamps, poi_start)
        timestamps = timestamps[idx_start:]

    if poi_end is not None:
        idx_stop = numpy.searchsorted(timestamps, poi_end, side='right')
        if idx_stop != len(timestamps):
            idx_stop += 1
        timestamps = timestamps[:idx_stop]

    return timestamps


def load_activity(csv_filename, num_uids, query_end_date, churn_time,
                  poi_start=None, poi_end=None, prediction_time=None):
    """
    reads data from a csv file, the first column of which is assumed to be the user's id (24 chars), and the following
    cells in the row store the corresponding timestamps at which any activity was logged for that user
    :param csv_filename: the path to the csv
    :param num_uids: mapping between string user ids and numeric user ids
    :param query_end_date: time after which we don't have any more data
    :param churn_time: how long a user has to be inactive to consider to have churned
    :param poi_start: If not None, used for pruning activity periods that don't overlap with a period of interest
    :param poi_end: If not None, used for pruning activity periods that don't overlap with a period of interest
    :param prediction_time: If not None, used for pruning activity periods that don't overlap with a period of interest.
                            Extends poi_end when looking for churns.
    :param trim_activity: if True, trim the start of each user's first output activity to the closest relevant
                          timestamp, if possible. I.e., if a user's first activity period starts before the poi, the
                          start point of that activity will be the last active timestamp before the poi, even if the
                          user had other activity before that, within the churn_time frame.
    :param trim_timestamps: when True, trims output user activities to the closest relevant timestamps. I.e., if a
                            user's first activity period starts before the poi, the start point of that activity will be
                            the last timestamp before the poi, even if the user did not churn before that. Similarly, if
                            an activity period ends after poi_end + prediction_time, its end timestamp will be the first
                            greater than poi_end + prediction_time, even if the user has continued to be active
                            afterwards.
    :return: a dictionary {num_uid: activity_periods}, where activity periods is a list of (start_date, stop_date)
             tuples, representing lapses of time during of which no two consecutive activity timestamps were more than
             churn_time apart.
             A user's first activity's start point may be trimmed to the user's last activity log entry's timestamp
             before poi_start.
             A user's last activity's end point can be 'nat' if we're unable to tell whether he has churned before
             query_end_date.
    """
    if poi_start is not None and poi_end is not None:
        assert numpy.datetime_data(poi_start) == numpy.datetime_data(poi_end)
        assert poi_start <= poi_end
    if poi_end is not None:
        assert query_end_date >= poi_end
    elif poi_start is not None:
        assert query_end_date >= poi_start

    # @TODO: one means to accelerate the load_activity function would be to trim by individual user timestamp limits
    #        of the score timestamps

    out = {}

    with open(csv_filename, newline='') as csv_file:
        reader = csv.reader(csv_file)
        for row_id, row in enumerate(reader):
            if not row:
                # skip empty rows
                continue

            uid = row[0]
            timestamps = row[1:]

            try:
                uid = num_uids[uid]
            except KeyError:
                # skip rows for which we don't have any engagement data
                continue

            if not timestamps:
                # skip empty lists of activity
                continue

            # try to free some memory before allocating a lot of new temp arrays
            gc.collect()

            # remove duplicates and sort timestamps
            timestamps = preproc_activity(timestamps, 'datetime64' if poi_start is None else poi_start.dtype)

            try:
                not_empty = bool(timestamps)
            except ValueError:
                # timestamps is a non-empty numpy array
                not_empty = True

            if not not_empty:
                # skip list of all empty cells
                continue

            # trim activity; we extend the poi by churn_time on both sides so as to be able to later determine whether
            # the user was active in the first / last day of the poi; also, if we want to make predictions for longer
            # periods (e.g., how many users will churn in the following year, even though a user is considered to have
            # churned after a month), we also take into account the prediction time

            lower_limit = None if poi_start is None else poi_start - churn_time

            if poi_end is None or prediction_time is None:
                upper_limit = None
            else:
                # we need to be able to figure out whether a user has churned during prediction_time. In order to do
                # that we need to take into account timestamps that happened
                upper_limit = poi_end + churn_time + prediction_time

            timestamps = trim_timestamps(timestamps, lower_limit)

            try:
                not_empty = bool(timestamps)
            except ValueError:
                # timestamps is a non-empty numpy array
                not_empty = True

            if not not_empty:
                # skip if there's nothing left after trim
                continue

            # convert timestamps to contiguous periods of activity
            activity = get_activity_periods(timestamps, query_end_date, churn_time)
            activity = trim_activity(activity, poi_start, poi_end)

            out[uid] = activity

    return out


def preproc_activity(timestamps_str, timestamps_dtype):
    # make a sorted numpy array of all timestamps; also remove duplicates

    # use a sorted set to avoid sorting later (might be a bit of an overkill, though); we use SortedSet instead
    # of SortedList to remove duplicates; ACTUALLY, using a sorted set is equivalent to applying an insertion sort,
    # which is slower than the sorting algorithm in numpy
    # @TODO: decide between implementation by profiling
    # timestamps = SortedSet()
    # for timestamp in timestamps_str:
    #     if not timestamp:
    #         # skip empty entries in the csv
    #         continue
    #     timestamps.add(numpy.datetime64(timestamp, time_unit))
    #
    # return timestamps

    # first implemetnation
    # timestamps = numpy.array(list(set(filter(lambda date_str: date_str != '', timestamps_str))), timestamps_dtype)
    # timestamps.sort()
    # return timestamps

    # should be faster
    # return numpy.unique(list(filter(lambda date_str: date_str != '', timestamps_str)), timestamps_dtype)

    # should be even faster
    if timestamps_dtype is None:
        timestamps_dtype = 'datetime64'  # unit is deduced from the strings
    timestamps = numpy.array(timestamps_str, timestamps_dtype)
    return numpy.unique(timestamps[~numpy.isnat(timestamps)])
