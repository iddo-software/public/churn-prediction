#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
try out various methods to read data from csv and time them. It's not a nice profiling method, but it serve the purpose
"""

from __future__ import print_function

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2018, InnerTrends & Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

from six import itervalues

import csv
import numpy
import timeit
import pandas as pd

try:
    import cPickle as pickle
except ImportError:
    import pickle

from churn_prediction.dataset import get_active_periods


def lines2timestamps(lines_iter, num_uids, split, timestamp_dtype):
    header_line = next(lines_iter)
    if split:
        header_line = header_line.strip().split(',')

    timestamps = numpy.empty((len(num_uids), len(header_line) - 1), timestamp_dtype)

    for line in lines_iter:
        if not line:
            # skip empty rows
            continue

        if split:
            line = line.strip().split(',')

        uid = line[0]

        try:
            num_uid = num_uids[uid]
        except KeyError:
            # skip rows for which we don't have any engagement data
            continue

        # if the uid has been found it means there was some engagement during the poi. If there is engagement there
        # should also be activity.
        # assert line[1:]

        timestamps[num_uid] = line[1:]

    return timestamps


def load_activity_raw_readlines(csv_filename, num_uids, timestamp_dtype=numpy.datetime64):
    with open(csv_filename, newline='') as csv_file:
        lines = iter(csv_file.readlines())

    return lines2timestamps(lines, num_uids, split=True, timestamp_dtype=timestamp_dtype)


def load_activity_raw_py(csv_filename, num_uids, timestamp_dtype=numpy.datetime64):
    with open(csv_filename, newline='') as csv_file:
        return lines2timestamps(csv_file, num_uids, split=True, timestamp_dtype=timestamp_dtype)


def load_activity_raw_csv(csv_filename, num_uids, timestamp_dtype=numpy.datetime64):
    with open(csv_filename, newline='') as csv_file:
        return lines2timestamps(csv.reader(csv_file), num_uids, split=False, timestamp_dtype=timestamp_dtype)


def select_valid_uids_numpy(data, invalid_uid):
    assert len(data.dtype.names) == 2
    uids, timestamps = data[data.dtype.names[0]], data[data.dtype.names[1]]
    valid_uid_mask = uids != invalid_uid
    valid_uids = uids[valid_uid_mask]

    assert len(valid_uids) <= invalid_uid

    uid_dtype = data.dtype[0]

    idx = numpy.arange(0, len(uids), dtype=uid_dtype)[valid_uid_mask]
    idx[valid_uids] = idx
    # idx[uid] is the position within uids at which uid can be found (uid is between 0 and len(num_uids))

    activity = timestamps[idx]

    return activity


def load_activity_raw_numpy(csv_filename, num_uids, loader, timestamp_dtype=numpy.datetime64):
    with open(csv_filename, newline='') as csv_file:
        reader = csv.reader(csv_file)
        field_names = next(reader)

    num_uid_dtype = numpy.dtype(type(next(itervalues(num_uids))))
    invalid_uid = numpy.iinfo(num_uid_dtype).max
    dtype = [(field_names[0], num_uid_dtype),
             ('timestamps', timestamp_dtype, len(field_names) - 1)]

    def str2num_uid(str_uid):
        try:
            return num_uids[str_uid]
        except KeyError:
            return invalid_uid

    if loader == numpy.genfromtxt:
        skip_header = 'skip_header'
    elif loader == numpy.loadtxt:
        skip_header = 'skiprows'
    else:
        raise NotImplementedError()

    kwargs = {skip_header: 1}

    data = loader(csv_filename, dtype, deKlimiter=',', converters={0: str2num_uid}, encoding=None, **kwargs)

    activity = select_valid_uids_numpy(data, invalid_uid)

    assert len(activity) == len(num_uids)
    return activity


def load_activity_raw_genfromtxt(csv_filename, num_uids, timestamp_dtype=numpy.datetime64):
    return load_activity_raw_numpy(csv_filename, num_uids, numpy.genfromtxt, timestamp_dtype)


def load_activity_raw_loadtxt(csv_filename, num_uids, timestamp_dtype=numpy.datetime64):
    return load_activity_raw_numpy(csv_filename, num_uids, numpy.loadtxt, timestamp_dtype)


def load_activity_raw_loadtxt_str_id(csv_filename, num_uids, timestamp_dtype=numpy.datetime64):
    with open(csv_filename, newline='') as csv_file:
        reader = csv.reader(csv_file)
        field_names = next(reader)
    # dtype = list(zip(field_names, chain([numpy.dtype('U24')], repeat(timestamp_dtype, len(field_names) - 1))))
    dtype = [(field_names[0], numpy.dtype('U24')), ('timestamps', timestamp_dtype, len(field_names) - 1)]

    activity = numpy.zeros((len(num_uids), len(field_names) - 1), timestamp_dtype)
    data = numpy.loadtxt(csv_filename, dtype, delimiter=',', skiprows=1)
    for str_uid, timestamps in data:
        try:
            num_uid = num_uids[str_uid]
        except KeyError:
            continue

        activity[num_uid] = timestamps

    return activity


def load_activity_raw_pandas(csv_filename, num_uids, timestamp_dtype=numpy.datetime64,
                             memory_map=False, read_index=False):
    with open(csv_filename, newline='') as csv_file:
        reader = csv.reader(csv_file)
        field_names = next(reader)

    num_uid_dtype = numpy.dtype(type(next(itervalues(num_uids))))
    invalid_uid = numpy.iinfo(num_uid_dtype).max
    uid_field_name = field_names[0]

    # dtype = {uid_field_name: num_uid_dtype}
    dtype = None  # pandas does not support both dtype and converter for the same column

    def str2num_uid(str_uid):
        try:
            return num_uids[str_uid]
        except KeyError:
            return invalid_uid

    converters = {uid_field_name: str2num_uid}

    infer_dates = True  # pandas doesn't support specifying date dtypes explicitly
    if not infer_dates:
        dtype = {}
        timestamp_field_names = field_names[1:]
        for timestamp_field_name in timestamp_field_names:
            dtype[timestamp_field_name] = timestamp_dtype

    df = pd.read_csv(csv_filename, header=0, index_col=0 if read_index else None, dtype=dtype, converters=converters,
                     parse_dates=list(range(1, len(field_names))) if infer_dates else False,
                     infer_datetime_format=infer_dates, memory_map=memory_map)

    # remove invalid values
    if read_index:
        # timeit(lambda: df.loc[range(len(df))], number=3)
        # Out[18]: 0.15973621400189586
        # timeit(lambda: df.sort_index(), number=3)
        # Out[19]: 0.1739891539909877
        # timeit(lambda: df.reindex(range(len(df)), copy=True), number=3)
        # Out[20]: 0.16876050700375345

        # # df = df[df.index.drop(invalid_uid)]  # not in-place
        # df.drop(invalid_uid, inplace=True)
        # df.sort_index(inplace=True)
        df = df.loc[range(len(num_uids))]

        # index probably not marked as unique here; use df.set_index(uid_field_name, inplace=True, verify_integrity=True)
    else:
        df = df[df[uid_field_name] != invalid_uid]
        # @TODO: instead of sort use something like select_valid_uids_numpy
        df.sort_values(by=uid_field_name, inplace=True)
        df.drop(uid_field_name, axis=1, inplace=True)

        # df.query could also work

        # the option of treating invalid_uid as na was considered, but discarded, as uint64 index can't hold NaNs
        # this option would use df.index.isnull()

        # df.filter only works on strings

        # duplicate entries in the num_uid column are invalid_ids, but duplicate detection is slower than dropping
        # invalid values
        # df = df[~df.index.duplicated(keep=False)]
        # df = df[df.index.drop_duplicates(keep=False)]

    return df.values.astype(timestamp_dtype)


def load_activity_raw_pandas_memmap(csv_filename, num_uids, timestamp_dtype=numpy.datetime64):
    load_activity_raw_pandas(csv_filename, num_uids, timestamp_dtype, memory_map=True)


def load_activity_raw_pandas_read_index(csv_filename, num_uids, timestamp_dtype=numpy.datetime64):
    load_activity_raw_pandas(csv_filename, num_uids, timestamp_dtype, read_index=True)


def load_activity_raw_pandas_memmap_read_index(csv_filename, num_uids, timestamp_dtype=numpy.datetime64):
    load_activity_raw_pandas(csv_filename, num_uids, timestamp_dtype, memory_map=True, read_index=True)


def load_activity_raw(csv_filename, num_uids, timestamp_dtype=numpy.datetime64):
    activity = load_activity_raw_csv(csv_filename, num_uids, timestamp_dtype)
    return activity


def get_all_activity_periods(timestamps, churn_time, poi_start=None, poi_end=None, trim_activity=False):
    activity_periods = numpy.zeros(len(timestamps), dtype=object)
    for row in timestamps:
        timestamps = numpy.unique(timestamps[~numpy.isnat(timestamps)])
        activity_periods[row[0]] = get_active_periods(row[1:], churn_time, poi_start, poi_end, trim_activity)

    return activity_periods


def load_activity(csv_filename, num_uids, churn_time, poi_start=None, poi_end=None, trim_activity=False):
    if poi_start is not None and poi_end is not None:
        assert numpy.datetime_data(poi_start) == numpy.datetime_data(poi_start)

    if poi_start is not None:
        timestamp_dtype = poi_start.dtype
    elif poi_end is not None:
        timestamp_dtype = poi_end.dtype
    else:
        timestamp_dtype = numpy.datetime64

    timestamps = load_activity_raw(csv_filename, num_uids, timestamp_dtype)
    activity = get_all_activity_periods(timestamps, churn_time, poi_start, poi_end, trim_activity)

    return activity


def main():
    points = 'data/innertrends_169.logbook_engagement_points.csv'
    activity = 'data/innertrends_169_activity.csv'
    with open(points + '.num_uids.cache', 'rb') as num_uids_cache_file:
        num_uids = pickle.load(num_uids_cache_file)

    repeat = 3
    number = 3

    for loader in [
        # load_activity_raw_pandas,
        # load_activity_raw_pandas_memmap,
        load_activity_raw_pandas_read_index,
        # load_activity_raw_pandas_memmap_read_index,
        # load_activity_raw_genfromtxt,
        # load_activity_raw_loadtxt,
        # load_activity_raw_loadtxt_str_id,
        load_activity_raw_readlines,
        load_activity_raw_py,
        load_activity_raw_csv
    ]:
        print(loader.__name__, ': %.2f' % (min(timeit.repeat(lambda: loader(activity, num_uids, timestamp_dtype='datetime64[D]'),
                                                             repeat=repeat, number=number)) / float(number)))


if __name__ == '__main__':
    main()
    # on the laptop:
    # load_activity_raw_pandas : 8.59
    # load_activity_raw_pandas_memmap : 8.47
    # load_activity_raw_pandas_read_index : 8.41 <--- NOTE: after removing sort, performance did not improve
    # load_activity_raw_pandas_memmap_read_index : 8.48
    # load_activity_raw_genfromtxt : 14.68
    # load_activity_raw_loadtxt : 7.49
    # load_activity_raw_loadtxt_str_id : 7.43
    # load_activity_raw_readlines : 2.19
    # load_activity_raw_py : 2.25
    # load_activity_raw_csv : 2.53
