#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
compute average churn rates for each engagement level
"""

from __future__ import print_function

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2018, InnerTrends & Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

import random
import numpy
from churn_prediction.dataset import load_data
from churn_prediction.dataset_preproc import engagement_scores, get_masks


def main():
    # data location and availability info

    # engagement data between 2017-02-01 and 2018-04-30 (151 days) for 23757 users
    points_filename = 'data/innertrends_169.logbook_engagement_points.csv'
    # TODO: have the user explicitly specify points_start_time, points_end_time, too

    # activity data between 2012-11-01 and 2018-06-13 for 32244 users (activity data ends 45 days after engagement data)
    activity_filename = 'data/innertrends_169_activity.csv'

    # time after which we don't have any more data (e.g., database query time)
    activity_end_time = numpy.datetime64('2018-06-13', 'D')
    # TODO: have the user explicitly specify the activity_start_time, too

    # points_cache_filename = points_filename + '.cache.npy'
    # num_uids_cache_filename = points_filename + '.num_uids.cache'
    # activity_cache_filename = activity_filename + '.cache.npy'
    # str_uids_cache_filename = points_filename + '.str_uids.cache.txt'

    points_cache_filename = num_uids_cache_filename = activity_cache_filename = str_uids_cache_filename = None

    # user params

    # we choose to read timestamps in the CSVs in the same time unit that will be used for computing scores to avoid
    # conversions later
    time_unit = 'D'  # 'D' for days
    score_window_size = numpy.timedelta64(30, 'D')
    churn_time = numpy.timedelta64(30, 'D')

    prediction_time = numpy.timedelta64(30, 'D')

    # thresholds between user engagement classes; intervals are open on the left and closed on the right
    score_thresholds = [0, 4, 418.93]
    engagement_levels = ['no', 'low', 'medium', 'high']

    print_some_multi_period_users = False

    print_extra_stats = False

    single_period_only = False

    # when reporting user churn rate, innertrends outputs the percentage of users that have churned within a month from
    # the number of users that were active at the beginning of said month. Thus, in order for the prediction to be
    # consistent with the report, we were thinking it might be a good idea to exclude from the statistics user-days for
    # which the users were not also active 30 days before. However, now I think it's a bad idea, but here is the code to
    # do it, if we decide otherwise, in the future
    exclude_inactive_before = False

    # ~user params

    # NOTE: both inside load_data and in engagement_scores, poi_start and poi_end are defined as the earliest
    # and, respectively, the latest timestamps encountered in the points dataset
    str_uids, points, active_periods = load_data(points_filename, activity_filename, time_unit, churn_time,
                                                 trim_active_periods=True, activity_window=None,
                                                 engagement_points_cache_filename=points_cache_filename,
                                                 num_uids_cache_filename=num_uids_cache_filename,
                                                 activity_cache_filename=activity_cache_filename,
                                                 str_uids_cache_filename=str_uids_cache_filename)

    if print_some_multi_period_users:
        multi_period_users = [str_uids[num_uid] for num_uid, user_periods in enumerate(active_periods) if len(user_periods) > 1]
        print(random.sample(multi_period_users, 5))
        # ['5a676bb8a5e649e4538c533d', '5a6773b3a5e649e4538d2b79', '5a676b46a5e649e4538be9eb', '5a677221a5e649e4538cdfb1', '5a676b30a5e649e4538bd348']

    # transform engagement points to engagement score matrix (users x timestamps)
    scores, poi_start = engagement_scores(points, numpy.timedelta64(score_window_size, time_unit))
    poi_end = poi_start + scores.shape[1] - 1
    # poi_end is guaranteed to be <= churns_upto because churns_upto was specified as poi_end when calling load_data

    # @TODO: a stronger sanity check would be to make sure that each day a user scored some engagement points was also
    # recorded in the activity log

    # get masks for scores array which indicate:
    #    - days when users were active
    #    - days when users were active, and the first churn time is less than prediction_time away
    #    - days when users were active, but can't be used for prediction because it's uncertain whether they precede a
    #      churn or not
    is_active, will_churn, unpredictive = get_masks(active_periods, activity_end_time, poi_start, poi_end, churn_time,
                                                    prediction_time, print_extra_stats)

    # sanity check
    assert numpy.all(will_churn == will_churn & is_active & ~unpredictive)

    score_thresholds = [None] + score_thresholds + [None]

    if single_period_only:
        single_period_users = numpy.array([len(user_periods) == 1 for user_periods in active_periods], bool)
        scores = scores[single_period_users]
        is_active = is_active[single_period_users]
        will_churn = will_churn[single_period_users]
        unpredictive = unpredictive[single_period_users]

    is_active &= ~unpredictive

    if exclude_inactive_before:
        is_active[:, churn_time:] &= is_active[:, :-churn_time]
        is_active[:, :churn_time] = False

    n_active = numpy.count_nonzero(is_active)

    # we don't do will_churn ~= unpredictive, because will_churn already contains only certain churns, and hence,
    # numpy.all(will_churn == will_churn & ~unpredictive)
    assert(numpy.all(will_churn == will_churn & ~unpredictive))

    if exclude_inactive_before:
        will_churn[:, churn_time:] &= is_active[:, :-churn_time]
        will_churn[:, :churn_time] = False

    n_will_churn = numpy.count_nonzero(will_churn)
    churn_rate = float(n_will_churn) / n_active
    print('{:,} users x'.format(len(is_active)), poi_end - poi_start + 1, '= {:,}'.format(scores.size),
          'entries, churn time = ' + str(churn_time) + ':')
    print('unpredictive entries:', '{:,}'.format(numpy.count_nonzero(unpredictive)))
    print('probability of a user to churn within ' + str(prediction_time) +
          ': {:.2%} ({:,} / {:,})'.format(churn_rate, n_will_churn, n_active))

    for engagement_level_idx, (lower_bound, upper_bound, engagement_level) \
            in enumerate(zip(score_thresholds[:-1], score_thresholds[1:], engagement_levels)):
        if lower_bound is not None:
            level_mask = scores > lower_bound
            if upper_bound is not None:
                level_mask &= scores <= upper_bound
        elif upper_bound is not None:
            level_mask = scores <= upper_bound
        else:
            continue  # no bounds, no print

        n_active = numpy.count_nonzero(is_active & level_mask)
        n_will_churn = numpy.count_nonzero(will_churn & level_mask)
        churn_rate = float(n_will_churn) / n_active

        print('probability of a user with', engagement_level, 'engagement to churn within ' + str(prediction_time) +
              ': {:.2%} ({:,} / {:,})'.format(churn_rate, n_will_churn, n_active))


if __name__ == '__main__':
    main()
    # original
    # probability of a user to churn within 30 days given his engagement class:
    # no engagement: 25.10%
    # low engagement: 18.78%
    # medium engagement: 3.72%
    # high engagement: 0.98%
    #
    # after invalidating uncertain non-churns with 'unpredictive' mask
    #
    # 23,767 users x 151 days = 3,588,817 entries, churn time = 30 days:
    # overall churn rate: 47.09% (11,193 / 23,767)  # output when print_extra_stats is True
    # unpredictive entries: 9,431
    # probability of a user to churn within 30 days: 11.82% (217,204 / 1,838,319)
    # probability of a user with no engagement to churn within 30 days: 25.25% (48,904 / 193,701)
    # probability of a user with low engagement to churn within 30 days: 18.93% (137,019 / 723,992)
    # probability of a user with medium engagement to churn within 30 days: 3.73% (30,182 / 808,640)
    # probability of a user with high engagement to churn within 30 days: 0.98% (1,099 / 111,986)
    #
    # 23,767 users x 151 days = 3,588,817 entries, churn time = 100 days:
    # overall churn rate: 18.94% (4,502 / 23,767)  # output when print_extra_stats is True
    # unpredictive entries: 89,235
    # probability of a user to churn within 30 days: 2.69% (56,915 / 2,116,542)
    # probability of a user with no engagement to churn within 30 days: 5.29% (19,732 / 373,216)
    # probability of a user with low engagement to churn within 30 days: 3.67% (30,110 / 820,258)
    # probability of a user with medium engagement to churn within 30 days: 0.82% (6,688 / 811,380)
    # probability of a user with high engagement to churn within 30 days: 0.34% (385 / 111,688)
    #
    # 23,767 users x 151 days = 3,588,817 entries, churn time = 120 days:
    # overall churn rate: 14.20% (3,374 / 23,767)  # output when print_extra_stats is True
    # unpredictive entries: 105,964
    # probability of a user to churn within 30 days: 1.87% (40,017 / 2,135,458)
    # probability of a user with no engagement to churn within 30 days: 3.62% (14,408 / 397,637)
    # probability of a user with low engagement to churn within 30 days: 2.56% (20,899 / 816,391)
    # probability of a user with medium engagement to churn within 30 days: 0.56% (4,552 / 809,969)
    # probability of a user with high engagement to churn within 30 days: 0.14% (158 / 111,461)
    #
    # ####################
    # single_period_only #
    # ####################
    #
    # 19,065 users x 151 days = 2,878,815 entries, churn time = 30 days:
    # unpredictive entries: 5,926
    # probability of a user to churn within 30 days: 6.27% (97,959 / 1,563,285)
    # probability of a user with no engagement to churn within 30 days: 12.33% (16,233 / 131,609)
    # probability of a user with low engagement to churn within 30 days: 11.35% (62,778 / 553,262)
    # probability of a user with medium engagement to churn within 30 days: 2.35% (18,069 / 767,795)
    # probability of a user with high engagement to churn within 30 days: 0.79% (879 / 110,619)
    #
    # 20,372 users x 151 days = 3,076,172 entries, churn time = 35 days:
    # unpredictive entries: 10,175
    # probability of a user to churn within 30 days: 5.95% (102,398 / 1,719,787)
    # probability of a user with no engagement to churn within 30 days: 12.08% (20,383 / 168,710)
    # probability of a user with low engagement to churn within 30 days: 9.85% (64,401 / 653,560)
    # probability of a user with medium engagement to churn within 30 days: 2.13% (16,765 / 786,574)
    # probability of a user with high engagement to churn within 30 days: 0.77% (849 / 110,943)
