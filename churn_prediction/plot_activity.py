#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
testing script
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2018, InnerTrends & Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

from __future__ import print_function

import numpy
from churn_prediction.dataset import load_data
from churn_prediction.dataset_preproc import engagement_scores, get_activity_count, get_activity_mask, get_masks

# data location and availability info

# engagement data between 2017-02-01 and 2018-04-30 (151 days) for 23757 users
points_filename = '../data/innertrends_169.logbook_engagement_points.csv'

# activity data between 2012-11-01 and 2018-06-13 for 32244 users (activity data ends 45 days after engagement data)
activity_filename = '../data/innertrends_169_activity.csv'

# time after which we don't have any more data (e.g., database query time)
activity_end_time = numpy.datetime64('2018-06-13', 'D')

points_cache_filename = num_uids_cache_filename = activity_cache_filename = str_uids_cache_filename = None

# user params

# we choose to read timestamps in the CSVs in the same time unit that will be used for computing scores to avoid
# conversions
time_unit = 'D'  # 'D' for days
score_window_size = numpy.timedelta64(30, 'D')
churn_time = numpy.timedelta64(30, 'D')

# thresholds between user engagement classes; intervals are open on the left and closed on the right
engagement_levels_score_thresholds = [0, 4, 418.93]
engagement_levels = ['no', 'low', 'medium', 'high']

# params for activity frequency analysis
activity_window = numpy.timedelta64(30, 'D')

# load and preproc

# NOTE: both inside load_data and in engagement_scores, poi_start and poi_end are defined as the earliest
# and, respectively, the latest timestamps encountered in the points dataset
str_uids, points, (active_periods, activity_timestamps) = \
    load_data(points_filename, activity_filename, time_unit, churn_time,
              trim_active_periods=True,
              activity_window=activity_window,
              engagement_points_cache_filename=points_cache_filename,
              num_uids_cache_filename=num_uids_cache_filename,
              activity_cache_filename=activity_cache_filename,
              str_uids_cache_filename=str_uids_cache_filename)

# transform engagement points to engagement score matrix (users x timestamps)
scores, poi_start = engagement_scores(points, numpy.timedelta64(score_window_size, time_unit))
poi_end = poi_start + scores.shape[1] - 1

activity_mask = get_activity_mask(activity_timestamps,
                                  numpy.datetime64(poi_start - activity_window, numpy.datetime_data(poi_start)),
                                  poi_end)

# convert to time unit in which data is available
activity_window_size = numpy.timedelta64(activity_window, time_unit)
# convert to int so we can index arrays with it
activity_window_size = numpy.int64(activity_window_size)

activity_count = get_activity_count(activity_mask, activity_window_size)
activity_count = activity_count[:, activity_window_size:]

is_active = get_masks(active_periods, activity_end_time, poi_start, poi_end, churn_time)

print(numpy.any(is_active & (activity_count == 0)))
