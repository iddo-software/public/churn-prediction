#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
another implementation of the churn rate computation algorithm
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2018, InnerTrends & Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

import numpy
import pickle


def get_churns(activity_filename, timestamp_dtype, churn_time, activity_end_time, drop_periods_after=None, uids=None):
    with open(activity_filename) as activity_file:
        _ = next(activity_file)  # skip header row
        users = 0
        churns = 0
        first_timestamp = None
        last_timestamp = None
        for row in activity_file:
            row = row.strip().split(',')
            uid = row[0]
            timestamps = numpy.asarray(row[1:], timestamp_dtype)
            timestamps = numpy.unique(timestamps[~numpy.isnat(timestamps)])
            if first_timestamp is None:
                first_timestamp = timestamps[0]
            else:
                first_timestamp = min(timestamps[0], first_timestamp)

            if last_timestamp is None:
                last_timestamp = timestamps[-1]
            else:
                last_timestamp = max(timestamps[-1], first_timestamp)

            if uids and uid not in uids:
                continue

            active_period_end = timestamps[-1]

            if drop_periods_after is not None:
                next_timestamp = timestamps[-1]
                for timestamp in timestamps[-2::-1]:
                    # we iterate over timestamps in reverse
                    if next_timestamp - timestamp > churn_time:
                        active_period_end = timestamp

                    if timestamp <= drop_periods_after:
                        break

                    next_timestamp = timestamp

            churned = active_period_end + churn_time <= activity_end_time
            churns += churned
            users += 1

    print('first timestamp:', first_timestamp)
    print('last timestamp:', last_timestamp)
    return users, churns


def main():
    activity_filename = 'data/innertrends_169_activity.csv'
    points_filename = 'data/innertrends_169.logbook_engagement_points.csv'

    time_unit = 'D'
    churn_time = numpy.timedelta64(30, 'D')
    activity_end_time = numpy.datetime64('2018-06-13')
    # poi_end = numpy.datetime64('2018-04-30')
    poi_end = None

    timestamp_dtype = numpy.datetime64('2018-04-30', time_unit).dtype

    with open(points_filename + '.num_uids.cache', 'rb') as num_uids_cache_file:
        num_uids = pickle.load(num_uids_cache_file)

    users = set(num_uids.keys())
    users = None

    n_users, n_churns = get_churns(activity_filename, timestamp_dtype, churn_time, activity_end_time, poi_end, users)

    print('overall churn rate: {:.2%} ({:,} / {:,})'.format(float(n_churns) / n_users, n_churns, n_users))


if __name__ == '__main__':
    main()
