#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
load data from csv
"""

from __future__ import print_function

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2018, InnerTrends & Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

from six.moves import xrange
from six import iteritems

import numpy
import gc
import csv
import os
from io import open

try:
    import cPickle as pickle
except ImportError:
    import pickle


def load_csv(csv_filepath, delimiter=',', skip_columns=None, field_dtypes=None, converters=None):
    # supports arbitrary column order in the csv

    # add converters for timestamps based on user selected dtype
    for field_name, field_dtype in iteritems(field_dtypes):
        if converters and field_name in converters:
            continue

        field_dtype = numpy.dtype(field_dtype)
        if numpy.issubdtype(field_dtype, numpy.datetime64):
            if converters is None:
                converters = {}

            time_unit = numpy.datetime_data(field_dtype)
            converters[field_name] = lambda time_str: numpy.datetime64(time_str, time_unit)

    # convert to set for fast lookup
    skip_columns = set(skip_columns)

    # read header to get column names (single line header assumed)
    with open(csv_filepath, 'rt') as csv_file:
        # this can also be done with next(csv.reader(csv_file))
        header = next(csv_file)
        # read the column names from the file; their dtype is supposed to be in 'field_dtype'
        col_names = str(header).strip().split(delimiter)

    # indices of the columns that we want to read (i.e., after excluding the ones we declared we wanted to skip)
    col_ids_keep = []

    # names of columns not skipped
    col_names_keep = []

    converters_by_col_id = {}

    for col_id, col_name in enumerate(col_names):
        if col_name not in skip_columns:
            col_ids_keep.append(col_id)
            col_names_keep.append(col_name)
            if col_name in converters:
                converters_by_col_id[col_id] = converters[col_name]

    dtype = [(col_name, field_dtypes[col_name]) for col_name in col_names_keep]

    # alternative: genfromtxt
    csv_data = numpy.loadtxt(csv_filepath, dtype, delimiter=delimiter, converters=converters_by_col_id, skiprows=1,
                             usecols=col_ids_keep)

    return csv_data


def transform_field(arr, field_name, field_dtype, tf):
    # turn arr's structured dtype into iterable
    new_dtype = [(field_name, field_dtype) if arr_field_name == field_name else (arr_field_name, arr.dtype[arr_field_name])
                 for arr_field_name in arr.dtype.names]

    col_names_except_replaced = list(arr.dtype.names)
    col_names_except_replaced.remove(field_name)

    new_arr = numpy.empty(arr.shape, new_dtype)

    new_arr[col_names_except_replaced] = arr[col_names_except_replaced]

    # for some reason, vectorize is slower than the for loop...
    # tf = numpy.vectorize(tf)
    # new_arr[field_name] = tf(arr[field_name])

    assert new_arr.ndim == 1

    for idx in xrange(len(new_arr)):
        new_arr[field_name][idx] = tf(arr[field_name][idx])

    return new_arr


def load_points(engagement_points_filename, time_unit):
    # load engagement points from csv

    # use dummy datetime object to extract dtype corresponding to time_unit
    time_dtype = numpy.datetime64('2018', time_unit).dtype
    # note: each column in the csv must be included either in the list of dtypes or in the list of skip cols below
    # specify the dtype of each column
    field_dtypes = {
        # max points observed (6113) would easily fit in uint16 (max=65535); we use uint32, though (max>4bn); actually,
        # we use uint64, as we're doing some cumsums later
        'points': numpy.uint64,
        'timestamp': time_dtype,  # force timestamp conversion to required time unit
        'uid': numpy.dtype('U24')  # we later convert this to uint32
    }

    skip_cols = ['"_id"']

    points = load_csv(engagement_points_filename, skip_columns=skip_cols, field_dtypes=field_dtypes)

    # @TODO: one 'U24' uid uses 96 bytes. If we read it as 'S24', it would take only 24. If we use bytes.fromhex as a
    # converter, it would only take 12 bytes ('b12' dtype); however, the uid column is converted to ints later, so this
    # only affects the memory footprint while loading the data (fyi, if you want to check the obtain the uid string
    # again, just call .hex() on the <bytes> object); actually, now load_data also returns the list of string ids, too,
    # but we only have <24K users, so memory is not critical for this thing right now (I think that things might be
    # sped down when looking up str_ids in the num_ids dict, because we'd have to convert them to bytes, first).

    return points


def first_nonzero_idx(arr):
    arr = arr.astype(numpy.bool)
    idx = numpy.argmax(arr)
    if not idx and not numpy.any(arr):
        return None
    return idx


def last_nonzero_idx(arr):
    idx = first_nonzero_idx(arr[::-1])
    if idx is None:
      return None
    idx = len(arr) - 1 - idx
    return idx


def get_active_periods(unique_timestamps, churn_time, poi_start=None, poi_end=None, trim_activity=False):
    # convert timestamps to contiguous periods of activity
    # for each user we make a list of (start, stop) for each active period (i.e., the dates between which the
    # lapse between any two consecutive activity log entries has not been longer than the churn time)
    # timestamps is assumed to be sorted and not to contain any duplicates
    #
    # if poi_start is None, the time unit is deduced from the strings

    timestamps = numpy.asarray(unique_timestamps)

    if timestamps.size == 0 or timestamps[0] > poi_end or timestamps[-1] < poi_start:
        # exit early if all activity is outside poi
        return None

    if poi_start is None:
        first_period_end_idx = 0
    else:
        first_period_end_idx = numpy.searchsorted(timestamps, poi_start)

    first_period_start = timestamps[first_period_end_idx]

    if first_period_end_idx:
        if trim_activity:
            prev_timestamp = timestamps[first_period_end_idx - 1]
            if first_period_start - prev_timestamp <= churn_time:
                first_period_start = prev_timestamp
        else:
            prev_timestamps = timestamps[first_period_end_idx::-1]  # reversed order, includes first_period_end
            churn_mask = prev_timestamps[1:] - prev_timestamps[:-1] > churn_time
            first_period_start_idx = first_nonzero_idx(churn_mask)
            if first_period_start_idx is None:
                first_period_start_idx = -1
            first_period_start = prev_timestamps[first_period_start_idx]
            # equivalent to this
            # for timestamp in prev_timestamps[1:]:
            #     if first_period_start - timestamp <= churn_time:
            #         first_period_start = timestamp
            #     else:
            #         break

    next_timestamps = timestamps[first_period_end_idx:]  # includes first_period_end
    churn_mask = next_timestamps[1:] - next_timestamps[:-1] > churn_time
    churn_indices = numpy.nonzero(churn_mask)[0]
    start_indices = churn_indices + 1
    churn_timestamps = next_timestamps[churn_indices]
    start_timestamps = next_timestamps[start_indices]

    if len(churn_timestamps):
        first_period_end = churn_timestamps[0]
        churn_timestamps[:-1] = churn_timestamps[1:]
        churn_timestamps[-1] = next_timestamps[-1]

        if poi_end is not None:
            cutoff_idx = first_nonzero_idx(start_timestamps > poi_end)
            if cutoff_idx is not None:
                start_timestamps = start_timestamps[:cutoff_idx]
                churn_timestamps = churn_timestamps[:cutoff_idx]
    else:
        # no churns happened, so first_period_end is also last_period_end, i.e., next_timestamps[-1]
        first_period_end = next_timestamps[-1]

    active_periods = numpy.empty((len(start_timestamps) + 1, 2), timestamps.dtype)
    active_periods[0][0] = first_period_start
    active_periods[0][1] = first_period_end
    active_periods[1:, 0] = start_timestamps
    active_periods[1:, 1] = churn_timestamps

    # equivalent to this:
    # active_periods = [[first_period_start, first_period_end]]
    # for timestamp in timestamps[first_period_end_idx + 1:]:
    #     if timestamp - active_periods[-1][-1] <= churn_time:
    #         active_periods[-1][-1] = timestamp  # extend last period
    #     elif poi_end is None or timestamp <= poi_end:
    #         active_periods.append([timestamp, timestamp])  # start new period
    #     else:
    #         break
    #
    # # keeps the time units of the timestamps (e.g., days)
    # active_periods = numpy.asarray(active_periods)
    # assert numpy.all(active_periods_numpy == active_periods)

    try:
        not_empty = bool(active_periods)
    except ValueError:
        # timestamps is a non-empty numpy array
        not_empty = True

    # if the uid has been found it means there was some engagement during the poi. If there is engagement there
    # should also be activity.
    assert not_empty

    return active_periods


def load_activity(csv_filename, churn_time, num_uids=None, poi_start=None, poi_end=None, trim_activity=False,
                  activity_window=None):
    """
    reads data from a csv file, the first column of which is assumed to be the user's id (24 chars), and the following
    cells in the row store the corresponding timestamps at which any activity was logged for that user
    :param csv_filename: the path to the csv
    :param num_uids: mapping between string user ids and numeric user ids
    :param churn_time: numpy.timedelta64 object specifying how long a user has to be inactive to consider to have
                       churned
    :param poi_start: If not None, a numpy.datetime64 object used for pruning activity periods that don't overlap with a
                      period of interest
    :param poi_end: If not None, a numpy.datetime64 object used for pruning activity periods that don't overlap with a
                    period of interest
    :param trim_activity: when True, trims output user activities to the closest relevant timestamps. I.e., if a user's
                          first active period starts before the poi, the start point of that activity will be replaced
                          with poi_start, even if the user had other activity before that within a churn_time-sized
                          time frame.
    :param activity_window: if not None, also return a sorted list of activity timestamps within
                            [poi_start - activity_window ... poi_end]
    :return: list of per-user activity periods. The index in the list is the user id, and the activity periods of a user
             are stored as a numpy array like a list of (start_date, stop_date) tuples, representing lapses of time
             during which no two consecutive activity log entry timestamps were more than churn_time apart. Optionally,
             if retun_timestamps is True, also returns activity timestamps. All outputs are converted to the time units
             of poi_start or poi_end (which ever is specified).

    """
    if poi_start is not None and poi_end is not None:
        assert numpy.datetime_data(poi_start) == numpy.datetime_data(poi_end)
        assert poi_start <= poi_end

    if poi_start is not None:
        timestamp_dtype = poi_start.dtype
    elif poi_end is not None:
        timestamp_dtype = poi_end.dtype
    else:
        timestamp_dtype = numpy.datetime64

    active_periods = numpy.zeros(len(num_uids), dtype=object)

    if activity_window is not None:
        activity_timestamps = numpy.zeros_like(active_periods)

    with open(csv_filename, newline='') as csv_file:
        reader = csv.reader(csv_file)
        for row_id, row in enumerate(reader):
            if not row:
                # skip empty rows
                continue

            uid = row[0]
            timestamps = row[1:]

            try:
                uid = num_uids[uid]
            except KeyError:
                # skip rows for which we don't have any engagement data
                continue

            # if the uid has been found it means there was some engagement during the poi. If there is engagement there
            # should also be activity.
            assert timestamps

            timestamps = numpy.array(timestamps, timestamp_dtype)

            # remove duplicates and sort timestamps
            timestamps = numpy.unique(timestamps[~numpy.isnat(timestamps)])

            # @TODO: timestamps after poi_end + prediction_time + churn_time can be dropped here

            if activity_window is not None:
                first_idx_in_poi = numpy.searchsorted(timestamps, poi_start - activity_window)
                first_idx_after_poi = numpy.searchsorted(timestamps, poi_end, 'right')
                activity_timestamps[uid] = timestamps[first_idx_in_poi: first_idx_after_poi]

            user_active_periods = get_active_periods(timestamps, churn_time, poi_start, poi_end, trim_activity)

            print_breaks = False
            if print_breaks and len(user_active_periods) > 1:
                for period1_end, period2_start in zip(user_active_periods[:-1, 1], user_active_periods[1:, 0]):
                    print('user', uid, '(row', str(row_id) + ') break:', period1_end, '-', period2_start)

            active_periods[uid] = user_active_periods

    if activity_window is not None:
        return active_periods, activity_timestamps
    else:
        return active_periods


def load_data(engagement_points_filename, activity_filename, time_unit='D', churn_time=numpy.timedelta64(30, 'D'),
              poi_start=None, poi_end=None, trim_active_periods=False, activity_window=None,
              engagement_points_cache_filename=None,
              num_uids_cache_filename=None, activity_cache_filename=None, str_uids_cache_filename=None):
    """
    reads data from csv files, and returns it as in-memory arrays. The data representation strives for compactness
    rather than ease of access. Check out functions in dataset_preproc for converting the raw data into formats that are
    easier to use for making predictions, but with higher memory footprint. This function should be roughly equivalent
    to a database query followed by mild postprocessing. This is just a thin wrapper over the functions load_points and
    load_activity, which adds some caching functionality for quicker debugging.
    :param engagement_points_filename: the path to the csv file containing engagement points data; it is expected to
                                       contain records with the following fields (after a header row containing the
                                       field names):
                                       - "_id": ignored
                                       - points: unsigned integer
                                       - timestamp: ISO format datetime data
                                       - uid: strings of 24 characters
    :param activity_filename: the path to the csv file containing activity data. After the header row, it is expected to
                              contain records the first field of which is the user id (corresponding to uid field of
                              engagement_points_filename) followed by an arbitrarily sized list of timestamps in which
                              the user has been spotted to have interacted with the app. The csv may contain empty cells
                              and no assumption is made regarding timestamp ordering
    :param time_unit: passed as the second argument of numpy.datetime64 or numpy.timedelta64 functions. Compatible
                      formats: a string, e.g., 'D' or '25h' or, equivalently, a tuple, e.g., ('D', 1) or ('h', 25). All
                      datetime objects are converted to this time unit
    :param churn_time: numpy.timedelta64 object representing how long a user needs to be inactive in order to consider
                       him to have churned. Default is 30 days
    :param poi_start: trim output sooner than this, if there is any
    :param poi_end: trim output later than this, if there is any
    :param trim_active_periods: check docstring of load_activity
    :param activity_window: check docstring of load_activity
    :param engagement_points_cache_filename: path to cache file for engagement points data, optional, default is None
    :param num_uids_cache_filename: path to cache file for mapping between string and numeric uids, optional, default is None
    :param activity_cache_filename: path to cache file for activity data, optional, default is None
    :param str_uids_cache_filename: path to cache file for list of uid strings, in order of their numeric uids (inverse
                                    mapping than what is stored in num_uids_cache_filename). This is never read, just
                                    written in plain text to help during debug.
    :return: (points, activity), where
             - points is a numpy array with fields "points"(uint64), "timestamp" (datetime64[<time_unit>]), "uid" (uint32)
             - activity is a list of per-user activity-periods. The index in the list is the user id, and the activity
               periods of a user is a numpy array equivalent to a list of (start_date, stop_date) tuples, representing
               lapses of time during of which no two consecutive activity timestamps were more than churn_time apart.
               For more details about it, check docstring of load_activity.
    """

    # steps for using a database instead of a csv file:
    # - user wants to run the analysis for a certain period of interest
    # - define poi_start and poi_end
    # - select users that were active during the poi:
    #   - if the poi is short, some users may be considered active during the poi, even if they had no actual activity,
    #     so what we need to do is this:
    #       - exclude users that have churned before the poi (i.e., users the last activity of which is before the poi
    #         and at least churn_time before the end of activity data)
    #       - exclude users the first activity of which was after the poi
    # - for these users, get engagement points between poi_start and poi_end
    # - for the same users, get activity data until poi_end + prediction_time + churn_time
    # - compute active_periods
    #   - this step can be combined with the previous one by mapping the function get_active_periods on the previous
    #     query's results, e.g., in the map step if the database framework offers map-reduce functionality, or as a
    #     stored procedure for sql databases

    # options for serialization:
    # * numpy only:
    # - arr.tofile / numpy.fromfile: not platform independent due to no endianness info
    # - numpy.save / numpy.load:
    #     - .npy for a single array
    #     - .npz for multiple arrays (multiple .npy's in an archive)
    #     - load also supports pickled files
    # - arr.dump: single array per file, pickled
    # * generic objects: pickle

    if engagement_points_cache_filename and os.path.isfile(engagement_points_cache_filename):
        points = numpy.load(engagement_points_cache_filename)
    else:
        # load data into a table with fields: points (uint64), timestamp('datetime64[D]'), uid(str[24])
        # this data needs conversion before being saved, which we can only do after getting the list of uids
        points = load_points(engagement_points_filename, time_unit)

    if num_uids_cache_filename and os.path.isfile(num_uids_cache_filename):
        with open(num_uids_cache_filename, 'rb') as num_uids_cache_file:
            num_uids = pickle.load(num_uids_cache_file)
        # str_uids = numpy.empty(len(num_uids), next(iterkeys(num_uids)).dtype)
        str_uids = numpy.asarray(num_uids.keys())

        # num_uids is, currently, unsorted (hash tables -> faster access)
        str_uid_idx = numpy.asarray(num_uids.values())
        str_uids[str_uid_idx] = str_uids
    else:
        # list of unique user ids, sorted lexicographically
        str_uids = numpy.unique(points['uid'])

        if len(str_uids) < numpy.iinfo(numpy.uint32).max:
            num_uid_dtype = numpy.uint32
        else:
            assert len(str_uids) < numpy.iinfo(numpy.uint64).max
            num_uid_dtype = numpy.uint64

        # convert string ids to numeric ids to save memory and improve performance

        # mapping between string ids and numeric ids
        num_uids = dict(zip(str_uids, numpy.arange(len(str_uids), dtype=num_uid_dtype)))
        if num_uids_cache_filename:
            with open(num_uids_cache_filename, 'wb') as num_uids_cache_file:
                pickle.dump(num_uids, num_uids_cache_file)

        if str_uids_cache_filename:
            with open(str_uids_cache_filename, 'wt') as str_uids_cache_file:
                str_uids_cache_file.writelines(str_uid + '\n' for str_uid in str_uids)

    if not engagement_points_cache_filename or not os.path.isfile(engagement_points_cache_filename):
        # convert 'uid' field from string to int
        points = transform_field(points, 'uid', numpy.uint32, lambda str_uid: num_uids[str_uid])

        # try to force free memory after reassigning points
        gc.collect()

        if engagement_points_cache_filename:
            numpy.save(engagement_points_cache_filename, points)

    # make sure engagement logs are sorted by timestamp
    # if numpy.any(points['timestamp'][1:] < points['timestamp'][:-1]):
    #     points.sort(points, order='timestamp')

    data_start_time = numpy.datetime64(points['timestamp'].min(), time_unit)
    data_end_time = numpy.datetime64(points['timestamp'].max(), time_unit)

    if poi_start is None or data_start_time > poi_start:
        poi_start = data_start_time
    else:
        poi_start = numpy.datetime64(poi_start, time_unit)
        points = points[points['timestamp'] >= poi_start]

    if poi_end is None or data_end_time < poi_end:
        poi_end = data_end_time
    else:
        poi_end = numpy.datetime64(poi_end, time_unit)
        points = points[points['timestamp'] <= poi_end]

    # load user activity logs
    if activity_cache_filename and os.path.isfile(activity_cache_filename):
        with open(activity_cache_filename, 'rb') as activity_cache_file:
            activity = pickle.load(activity_cache_file)
    else:
        # load_activity generates a list of activity periods; note, however, that, if trim_activity is True, the first
        # activity period's start time might be later than the true one, as dates too old to be relevant for the poi are
        # dropped for lower memory footprint and faster processing
        activity = load_activity(activity_filename, churn_time, num_uids, poi_start, poi_end, trim_active_periods,
                                 activity_window)

        if activity_cache_filename:
            with open(activity_cache_filename, 'wb') as activity_cache_file:
                pickle.dump(activity, activity_cache_file)

    # sanity check: load_activity makes sure that activity uids are included score uids, but we also check that
    # score uids are included activity uids
    if activity_window is not None:
        active_periods, activity_timestamps = activity
        assert len(activity_timestamps) == len(num_uids)
    else:
        active_periods = activity
    assert len(active_periods) == len(num_uids)

    return str_uids, points, activity

    # TODO: check what happens if churn_time / prediction_time is shorter than time_unit
