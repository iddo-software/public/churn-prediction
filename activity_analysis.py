#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
visualize and compute statistics of user activity data
"""

from __future__ import print_function

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2018, InnerTrends & Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

try:
    # relevant for setting the bandwidth when plotting kdes
    import statsmodels
except ImportError:
    has_statsmodels = False
else:
    has_statsmodels = True

# generic imports
import os
import numpy
from collections import OrderedDict
import webbrowser

# imports for plotting
import matplotlib; matplotlib.use('webagg')
from matplotlib import pyplot as plt
import seaborn as sns; sns.set(style='whitegrid')  # switch to seaborn looks (default style=darkgrid)
import mpld3
from plotly.offline.offline import get_plotlyjs

# local imports
from churn_prediction.dataset import load_data, first_nonzero_idx
from churn_prediction.dataset_preproc import engagement_scores, get_activity_count, get_activity_mask, get_masks
from util_stats import custom_bxpstats, hists_independent, get_level_activity_limits


def get_level_indicator_mask(scores, engagement_levels_score_thresholds):
    # matrix where each entry contains a numeric label representing the level of engagement of a user in a day
    # E.g.: if level_indicator_mask[3][15] == 1, it means that user with id 3 was marked as having
    # low engagement on the 16th day; int8 because we assume there are no more than 128 engagement levels
    level_indicator_mask = numpy.full(scores.shape, -1, numpy.int8)

    thresholds = [None] + engagement_levels_score_thresholds + [None]

    for level_idx, (lower_bound, upper_bound) in enumerate(zip(thresholds[:-1], thresholds[1:])):
        if lower_bound is not None:
            level_mask = scores > lower_bound
            if upper_bound is not None:
                level_mask &= scores <= upper_bound
        elif upper_bound is not None:
            level_mask = scores <= upper_bound
        else:
            continue  # no bounds, no print

        level_indicator_mask[level_mask] = level_idx

    return level_indicator_mask


def get_closed_interval_length_mask(activity_mask):
    # uint16 because break times are < 180 years
    out = numpy.zeros(activity_mask.shape + (2,), numpy.uint16)
    for user_activity, user_out in zip(activity_mask, out):
        activity_indices = numpy.nonzero(user_activity)
        for first_idx, last_idx in zip(activity_indices[:-1], activity_indices[1:]):
            user_out[first_idx: last_idx, 0] = last_idx - first_idx + 1
            user_out[last_idx, 1] = last_idx - first_idx + 1

    return out


def get_activity(points_filename, activity_filename, time_unit, churn_time, activity_window, score_window,
                 activity_end_time, engagement_levels_score_thresholds, active_means_engaged, print_stats):
    """

    :param points_filename:
    :param activity_filename:
    :param time_unit:
    :param churn_time:
    :param activity_window: if None, analyse breaks instead of frequencies
    :param score_window:
    :param activity_end_time:
    :param engagement_levels_score_thresholds:
    :param active_means_engaged:
    :param print_stats:
    :return:
    """
    # NOTE: both inside load_data and in engagement_scores, poi_start and engagement_end are defined as the earliest
    # and, respectively, the latest timestamps encountered in the points dataset

    if activity_window is None:
        raise NotImplementedError('break analysis not yet implemented')
    # @TODO: if we analyse breaks, we should also load activity timestamps that are further than churn_time before
    #        poi_start
    str_uids, points, activity = load_data(points_filename, activity_filename, time_unit, churn_time,
                                           trim_active_periods=activity_window is not None,
                                           activity_window=None if active_means_engaged else activity_window)

    # transform engagement points list to engagement score and points matrices (users x timestamps)
    scores, engagement_start, points = engagement_scores(points, numpy.timedelta64(score_window, time_unit),
                                                  ret_points_matrix=True)
    engagement_end = engagement_start + scores.shape[1] - 1

    # activity[0] is active_periods if activity_window is not None
    is_active = get_masks(activity if active_means_engaged else activity[0], activity_end_time, engagement_start,
                          engagement_end, churn_time)

    if active_means_engaged:
        activity_start = engagement_start
        activity_mask = numpy.asarray(points, dtype=bool)
    else:
        activity_start = engagement_start - numpy.timedelta64(activity_window, time_unit)
        # activity[1] is activity_timestamps if activity_window is not None
        activity_mask = get_activity_mask(activity[1], activity_start, engagement_end)

    # convert to int so we can index arrays with it
    activity_window_size = numpy.int64(numpy.timedelta64(activity_window, time_unit))
    if activity_window is None:
        pass
    else:
        activity_count_mask = get_activity_count(activity_mask, activity_window_size)

    activity_count_mask = activity_count_mask[:, activity_window_size:]

    if active_means_engaged:
        scores = scores[:, activity_window_size:]
        is_active = is_active[:, activity_window_size:]

    level_mask = get_level_indicator_mask(scores, engagement_levels_score_thresholds)

    # only take into account days in which users were active
    activity = activity_count_mask[is_active]
    levels = level_mask[is_active]

    level_activity = [activity[levels == level] for level in range(len(engagement_levels_score_thresholds) + 1)]

    if print_stats:
        print(len(scores), 'users\n',
              'engagement data between', engagement_start, 'and', engagement_end, '(', engagement_end - engagement_start + 1, ')\n',
              'activity data between', activity_start, 'and', engagement_end, '(', engagement_end - activity_start + 1, ')\n',
              activity_count_mask.shape[1], 'windows of', activity_window, '\n',
              numpy.count_nonzero(is_active), 'active windows')

    return activity, level_activity


def get_activity_hists(activity_counts, level_activity_counts):
    # data as histograms
    activity_hist = numpy.bincount(activity_counts)
    lowest_activity = first_nonzero_idx(activity_hist)  # = 1 (we only take active users into account, churn_time==activity_window_size, so there are no 0s)
    activity_hist = activity_hist[lowest_activity:]

    level_activity_hists = numpy.zeros((len(level_activity_counts), len(activity_hist)), numpy.int64)

    for level_counts, act_hist in zip(level_activity_counts, level_activity_hists):
        hist = numpy.bincount(level_counts)[lowest_activity:]
        act_hist[:len(hist)] = hist

    # for seaborn
    activity_vals = numpy.arange(lowest_activity, lowest_activity + len(activity_hist))
    activity_bins = numpy.arange(lowest_activity - .5, lowest_activity + len(activity_hist) + 1, 1)

    return activity_hist, level_activity_hists, activity_vals, activity_bins


def get_plot_divs(activity_counts, level_activity_counts, activity_vals, activity_hist, level_activity_hists,
                  activity_bins, activity_window, engagement_levels, level_colors, level_activity_limits, bxpstats_all,
                  bxpstats, indep_level_pairs, confidence, short_html=False, debug_html_size=False):
    import plotly.graph_objs as go
    # from plotly.grid_objs import Column, Grid
    from plotly.tools import make_subplots
    import plotly.offline as py

    plot_divs = OrderedDict()

    def plot(fig, name=None):
        div = py.plot(fig, show_link=False, validate=False, output_type='div', include_plotlyjs=False)
        if debug_html_size:
            print(name, 'plotly: %.2f' % (len(div) / (1024. * 1024)))
        if name:
            plot_divs[name] = div

    def plot_mpl(fig, name=None):
        #  strip_style=True is broken in plotly
        plt.close(fig)
        div = py.plot_mpl(fig, resize=True, show_link=False, validate=False, output_type='div', include_plotlyjs=False)
        if debug_html_size:
            print(name, 'plotly mpl: %.2f' % (len(div) / (1024. * 1024)))
        if name:
            plot_divs[name] = div

    # mpld3
    def show(fig, name=None, legend=False, containers=False):
        """
        param fig: a matplotlib figure
        """
        plt.close(fig)
        plugins = [mpld3.plugins.MousePosition()]  # mpld3.plugins.LineHTMLTooltip
        if legend:
            for ax in fig.axes:
                handles, labels = ax.get_legend_handles_labels()  # return lines and labels
                if containers:
                    els = [[handle] + container.get_children() for handle, container in zip(handles, ax.containers)]
                else:
                    els = zip(handles, ax.collections)
                plugins.append(mpld3.plugins.InteractiveLegendPlugin(els, labels, ax, alpha_unsel=0.5, alpha_over=1.5,
                                                                     start_visible=True))

        mpld3.plugins.connect(fig, *plugins)
        div = mpld3.fig_to_html(fig)
        if debug_html_size:
            print(name, 'mpld3: %.2f' % (len(div) / (1024. * 1024)))
        if name:
            plot_divs[name] = div

    lowest_activity = activity_vals[0]

    ###################################
    # Plots for all engagement levels #
    ###################################

    hist_x_str = '# Active Days Within Windows of ' + str(activity_window)
    hist_y_str = '# ' + str(activity_window) + ' long windows'

    # Histogram (Plotly)
    if debug_html_size or short_html:
        fig_mpl = plt.figure()
        ax = sns.distplot(activity_vals,
                          bins=activity_bins,
                          hist=True, rug=False, kde=False,
                          hist_kws=dict(weights=activity_hist),
                          # norm_hist=True,
                          axlabel=hist_x_str,
                          label='All Engagement Levels')
        # ax.legend()

        # plt.subplots_adjust(left=0.15)

        plt.title('Histogram (All Engagement Levels)')
        # plt.xlabel("# active days over 30-day windows")
        plt.ylabel(hist_y_str)

    if debug_html_size or not short_html:
        trace = go.Histogram(
            x=activity_counts,
            # xsrc=grid[0],
            histnorm='percent',
            name='all users'
        )

        layout = go.Layout(
            title='Activity frequency',
            xaxis=dict(
                title=hist_x_str,
                # tickformat='.2f',
                hoverformat='.2f',
            ),
            yaxis=dict(
                title='% of windows of ' + str(activity_window),
                hoverformat='.2f',
            ),
            bargap=0.2,
            # optional, but make plots look good in jupyter lab
            height=600,
            width=800,
        )

        fig = go.Figure(data=[trace], layout=layout)

    if short_html:
        if debug_html_size:
            plot(fig, 'hist')
            show(fig_mpl, 'hist')
        plot_mpl(fig_mpl, 'hist')
    else:
        if debug_html_size:
            plot_mpl(fig_mpl, 'hist')
            show(fig_mpl, 'hist')
        plot(fig, 'hist')

    # Histogram + kde (seaborn)

    # underlying seaborn.kde does not support pre-counted histograms...
    fig = plt.figure()
    bw = .7 if has_statsmodels else .1
    cut = 4 if has_statsmodels else 1. / bw
    sns.distplot(activity_counts,
                 bins=activity_bins,
                 hist=True, rug=False, kde=True,
                 # kde_kws=dict(kernel='epa', bw=5, cut=0, label='kde', clip=(lowest_freq - 1, highest_freq + 1)),
                 kde_kws=dict(kernel='gau', bw=bw, cut=cut, label='kde',
                              clip=(lowest_activity - 1, lowest_activity + len(activity_bins) - 2)),
                 axlabel=hist_x_str,
                 label='All Engagement Levels')

    # plt.subplots_adjust(left=0.15)

    plt.title("Normed Histogram and KDE (All Engagement Levels)")
    # plt.xlabel("# active days over 30 day windows")
    plt.ylabel("Probability")

    # plot_mpl(fig)  # Conversion of kde to plotly fails.
    show(fig, 'hist_kde')

    if debug_html_size or short_html:
        # Violin plot (seaborn)
        fig_mpl = plt.figure()
        bw = .1
        sns.violinplot(data=activity_counts, bw=bw, cut=1. / bw, width=.5)

        plt.title('Violin Plot (All Engagement Levels)')
        plt.xlabel('Probability')
        plt.ylabel(hist_x_str)

    if debug_html_size or not short_html:
        # Violin plot (native plotly) v2
        trace = go.Violin(
            y=activity_counts,
            bandwidth=.7,
            spanmode='hard',
            box=go.violin.Box(visible=True),
            line=go.violin.Line(color='black'),
            meanline=go.violin.Meanline(visible=True),
            name='All Engagement Levels',
            points=False,  # not showing each individual outlier makes the graph more responsive
        )

        layout = go.Layout(
            title=hist_x_str,
            yaxis=dict(zeroline=False),
            # optional, but make plots look good in jupyter lab
            height=600,
            width=800,
        )

        fig = go.Figure(data=[trace], layout=layout)

    if short_html:
        if debug_html_size:
            plot(fig, 'violin')
        show(fig_mpl, 'violin')
    else:
        if debug_html_size:
            show(fig_mpl, 'violin')
        plot(fig, 'violin')

    if debug_html_size or short_html:
        # Box plot (matplotlib)
        fig_mpl = plt.figure()
        ax = plt.gca()

        notches = ('cilo' in bxpstats_all)
        assert notches == ('cihi' in bxpstats_all)

        ax.bxp([bxpstats_all],
               shownotches=notches,
               showmeans=True,
               showfliers=False,  # don't show outliers with mpld3
               meanline=True)

        plt.title("Box plot (All Engagement Levels)")
        plt.xlabel("Engagement Level")
        plt.ylabel(hist_x_str)

    if debug_html_size or not short_html:
        # Box plot
        trace = go.Box(
            y=activity_counts,
            name='All Engagement Levels',
            line=go.box.Line(color='black'),
            marker=go.box.Marker(opacity=0),
            # too many outliers, so we don't plot them (setting boxpoints to False changes whiskers)
            boxmean='sd',
            boxpoints='outliers',
            notched=True,  # median confidence interval is infinitesimally tight, so it doesn't show
        )

        layout = go.Layout(
            title=hist_x_str,
            yaxis=dict(zeroline=False),
            # optional, but make plots look good in jupyter lab
            height=600,
            width=800,
        )

        fig = go.Figure(data=[trace], layout=layout)

    if short_html:
        if debug_html_size:
            plot(fig, 'box')
        show(fig_mpl, 'box')
    else:
        if debug_html_size:
            show(fig_mpl, 'box')
        plot(fig, 'box')

    # Boxplot stats
    stats_table = OrderedDict()
    stats_table['min (q0)'] = numpy.min(activity_counts)
    stats_table['lower fence'] = '%.2f' % bxpstats_all['whislo']
    stats_table['q1'] = bxpstats_all['q1']
    stats_table['inlier median (q2)'] = bxpstats_all['med']
    if 'cilo' in bxpstats_all and 'cihi' in bxpstats_all:
        stats_table['inlier median 95% confidence interval'] = bxpstats_all['cilo'], bxpstats_all['cihi']
    stats_table['inlier mean'] = '%.2f' % bxpstats_all['mean']
    stats_table['q3'] = bxpstats_all['q3']
    stats_table['upper fence'] = '%.2f' % bxpstats_all['whishi']
    stats_table['max (q4)'] = numpy.max(activity_counts)
    n_outliers = len(bxpstats_all['fliers'])
    stats_table['outliers'] = '{0:,}'.format(n_outliers) + ' (%.2f%%)' % (n_outliers * 100. / len(activity_counts))

    fig = go.Figure(data=[go.Table(header=dict(values=['Statistic', 'All Engagement Levels'],
                                               fill=dict(color='#AAAAAA')),
                                   cells=dict(values=[list(stats_table.keys()), list(stats_table.values())]))],
                    layout=go.Layout(# width=800,
                                     # autosize=False,
                                     title='Box Plot Statistics')
                    )

    plot(fig, 'boxplot_stats')

    #########################################
    # Individual plots per engagement level #
    #########################################

    # Histograms

    if debug_html_size or short_html:
        fig_mpl, axes = plt.subplots(nrows=len(engagement_levels), ncols=1, sharex='all', sharey='all', squeeze=True,
                                     figsize=(7, 15))
        for level_idx, (engagement_level, ax, color) in enumerate(zip(engagement_levels, axes, level_colors)):
            sns.distplot(
                activity_vals,
                activity_bins,
                hist=True, kde=False, rug=False,
                hist_kws=dict(weights=level_activity_hists[level_idx]),
                color=color,
                norm_hist=False,
                # axlabel=engagement_level,  # looks bad
                label=engagement_level,
                ax=ax,
            )

            ax.set_title(engagement_level + ' Engagement')
            # ax.set_xlabel("# active days within 30-day windows")  # looks bad
            ax.set_ylabel(hist_y_str)

        fig_mpl.suptitle('Histograms Per Engagement Level')  # doesn't appear to work

    if debug_html_size or not short_html:
        traces = []
        for engagement_level, color, level_counts in zip(engagement_levels, level_colors, level_activity_counts):
            trace = go.Histogram(
                x=level_counts,
                # we don't use histnorm='percent, because we would like to keep the heights of the bars proportional
                opacity=.5,
                name=engagement_level + ' Engagement',
                marker=go.histogram.Marker(color=color)
            )

            traces.append(trace)

        fig = make_subplots(rows=len(engagement_levels), cols=1)
        for trace_idx, trace in enumerate(traces):
            fig.append_trace(trace, trace_idx + 1, 1)

        fig['layout'].update(height=800, width=600, title=hist_x_str)

    if short_html:
        if debug_html_size:
            plot(fig, 'hists')
            show(fig_mpl, 'hists')
        plot_mpl(fig_mpl, 'hists')
    else:
        if debug_html_size:
            plot_mpl(fig_mpl, 'hists')
            show(fig_mpl, 'hists')
        plot(fig, 'hists')

    # Overlaid Histograms

    if debug_html_size or not short_html:
        layout = go.Layout(
            title=hist_x_str,
            barmode='overlay',
            bargap=.2,
            # optional, but make plots look good in jupyter lab
            height=600,
            width=800,
        )

        fig = go.Figure(data=traces, layout=layout)

    if debug_html_size or short_html:
        fig_mpl = plt.figure()
        original_fig_size = fig_mpl.get_size_inches()
        # fig_mpl.set_size_inches(12, 7)

        for level_idx, (engagement_level, color) in enumerate(zip(engagement_levels, level_colors)):
            sns.distplot(
                activity_vals,
                activity_bins,
                hist=True, kde=False, rug=False,
                hist_kws=dict(weights=level_activity_hists[level_idx]),
                color=color,
                norm_hist=False,
                # axlabel='# active days in 30 day windows',
                label=engagement_level + ' Engagement')
        ax = plt.gca()

        plt.title("Overlaid Histograms")
        plt.xlabel(hist_x_str)
        plt.ylabel(hist_y_str)

        plt.subplots_adjust(left=0.15)

    if short_html:
        if debug_html_size:
            plot(fig, 'hists_overlaid_interactive')
        plot_mpl(fig_mpl, 'hists_overlaid_interactive')  # fails with legend
        fig_mpl.set_size_inches(*original_fig_size)
        ax.legend()
        show(fig_mpl, 'hists_overlaid')
    else:
        if debug_html_size:
            plot_mpl(fig_mpl, 'hists_overlaid_interactive')  # fails with legend
            fig_mpl.set_size_inches(*original_fig_size)
            ax.legend()
            show(fig_mpl, 'hists_overlaid')
        plot(fig, 'hists_overlaid_interactive')

    # limits of activity per engagement level

    thr_strs = []
    for engagement_level, (lo, hi) in zip(engagement_levels, level_activity_limits):
        if lo is not None:
            thr_strs.append('User engagement level is likely to be at least ' + engagement_level
                            + ' if he is active more than ' + str(lo - 1) + ' of ' + str(activity_window))
        if hi is not None:
            thr_strs.append('user engagement level is likely to be at most ' + engagement_level
                            + ' if he is active less than ' + str(hi + 1) + ' of ' + str(activity_window))

    fig = go.Figure(data=[go.Table(header=dict(values=['thresholds']), cells=dict(values=[thr_strs]))],
                    layout=go.Layout(width=800, autosize=False, title='Engagement Thresholds'))
    plot(fig, 'thresholds')

    # Overlaid histograms (normed)
    if debug_html_size or not short_html:
        traces = []
        for engagement_level, color, level_counts in zip(engagement_levels, level_colors, level_activity_counts):
            trace = go.Histogram(
                x=level_counts,
                histnorm='percent',
                opacity=.5,
                name=engagement_level + ' Engagement',
                marker=go.histogram.Marker(color=color)
            )

            traces.append(trace)

        fig = go.Figure(data=traces, layout=layout)

    if debug_html_size or short_html:
        fig_mpl = plt.figure()
        original_fig_size = fig_mpl.get_size_inches()
        # fig_mpl.set_size_inches(12, 7)

        for level_idx, (engagement_level, color) in enumerate(zip(engagement_levels, level_colors)):
            sns.distplot(
                activity_vals,
                activity_bins,
                hist=True, kde=False, rug=False,
                hist_kws=dict(weights=level_activity_hists[level_idx]),
                color=color,
                norm_hist=True,
                # axlabel='# active days in 30 day windows',
                label=engagement_level + ' Engagement')
        ax = plt.gca()

        plt.title("Overlaid Normed Histograms")
        plt.xlabel(hist_x_str)
        plt.ylabel('Probability')

    if short_html:
        if debug_html_size:
            plot(fig, 'hists_overlaid_interactive_normed')
        plot_mpl(fig_mpl, 'hists_overlaid_interactive_normed')  # fails with legend
        fig_mpl.set_size_inches(*original_fig_size)
        ax.legend()
        show(fig_mpl, 'hists_overlaid_normed')
    else:
        if debug_html_size:
            plot_mpl(fig_mpl, 'hists_overlaid_interactive_normed')  # fails with legend
            fig_mpl.set_size_inches(*original_fig_size)
            ax.legend()
            show(fig_mpl, None)
        plot(fig, 'hists_overlaid_interactive_normed')

    # Overlaid KDEs
    fig = plt.figure()
    bw = .6 if has_statsmodels else .15
    cut = 3 if has_statsmodels else 1. / bw

    for level, color, level_counts in zip(engagement_levels, level_colors, level_activity_counts):
        sns.kdeplot(
            level_counts,
            shade=True,
            kernel='gau',
            bw=bw,
            # gridsize ?
            cut=cut,
            clip=(lowest_activity - 1, lowest_activity + len(activity_bins) - 2),
            legend=True,
            shade_lowest=False,
            color=color,
            label=level + ' engagement'
        )

    plt.title("Overlaid KDEs")
    plt.xlabel(hist_x_str)
    plt.ylabel("Probability")

    show(fig, 'kdes_overlaid')
    # plot_mpl(fig, 'kdes_overlaid interactive')  # fails

    # Violin plots
    if debug_html_size or not short_html:
        traces = []
        for engagement_level, color, level_counts in zip(engagement_levels, level_colors, level_activity_counts):
            trace = go.Violin(
                y=level_counts,
                bandwidth=.7,
                spanmode='hard',
                box=go.violin.Box(visible=True),
                line=go.violin.Line(color=color),
                meanline=go.violin.Meanline(visible=True),
                name=engagement_level + ' Engagement',
                points=False,  # not showing each individual outlier makes the graph more responsive
                # turn these two on to make the violins' relative widths proportional to population sizes;
                # this, however, makes the boxes harder to see
                # scalemode='count',
                # scalegroup='group_id',
                # relevant for split violins:
                # side='positive' / 'negative'
            )

            traces.append(trace)

        layout = go.Layout(
            title=hist_x_str,
            yaxis=dict(zeroline=False),
            # optional, but make plots look good in jupyter lab
            height=500,
            width=800,
        )

        fig = go.Figure(data=traces, layout=layout)

    if debug_html_size or short_html:
        # Violin plots (seaborn)
        fig_mpl = plt.figure()
        sns.violinplot(
            data=level_activity_counts,
            # x=[level + ' engagement' for level in engagement_levels],
            # y=level_activity_counts,
            bw=.5,
            cut=0,
            scale='count',
            palette=level_colors,
        )

        # plt.subplots_adjust(left=0.15)

        plt.title("Violin plots")
        plt.xlabel("Engagement Levels")
        plt.ylabel(hist_x_str)

        # @TODO: change ticks of x axis

    if short_html:
        if debug_html_size:
            plot(fig, 'violins')
        show(fig_mpl, 'violins')
    else:
        if debug_html_size:
            show(fig_mpl, 'violins')
        plot(fig, 'violins')

    # Box plots
    if debug_html_size or not short_html:
        traces = []
        for engagement_level, color, level_counts in zip(engagement_levels, level_colors, level_activity_counts):
            trace = go.Box(
                y=level_counts,
                name=engagement_level + ' Engagement',
                line=go.box.Line(color=color),
                boxmean='sd',
                boxpoints='outliers',
                marker=go.box.Marker(opacity=0),
                # too many outliers, so we don't plot them (setting boxpoints to False changes whiskers)
                notched=True,  # doesn't work
            )

            traces.append(trace)

        layout = go.Layout(
            title=hist_x_str,
            yaxis=dict(zeroline=False),
            # optional, but make plots look good in jupyter lab
            height=600,
            width=800,
        )

        fig = go.Figure(data=traces, layout=layout)

    if debug_html_size or short_html:
        fig_mpl = plt.figure()
        plt.boxplot(level_activity_counts, notch=True, sym='',
                    labels=[level + ' Engagement' for level in engagement_levels], meanline=True, showmeans=True,
                    showfliers=False)

        plt.title('Box Plots')
        plt.xlabel('Engagement Level')
        plt.ylabel(hist_x_str)

    if not short_html:
        if debug_html_size:
            show(fig_mpl, 'boxes')
        plot(fig, 'boxes')
    else:
        if debug_html_size:
            plot(fig, 'boxes')
        show(fig_mpl, 'boxes')

    # Custom boxplots
    fig = plt.figure()
    ax = plt.gca()

    notches = None
    for stats in bxpstats:
        if notches is None:
            notches = 'cilo' in stats
        else:
            assert notches == ('cilo' in stats)
        assert ('cilo' in stats) == ('cihi' in stats)

    ax.bxp(bxpstats,
           shownotches=notches,
           showmeans=True,
           showfliers=False,  # don't show outliers with mpld3
           meanline=True)

    plt.title('Custom Box Plots')
    plt.xlabel('Engagement Level')
    plt.ylabel(hist_x_str)

    show(fig, 'custom_boxes')

    # Boxplot stats
    stat_names = ['min (q0)', 'lower fence', 'lower bound of heavy range', 'inlier median (q2)']
    if notches:
        stat_names.append('inlier median 95% confidence interval')
    stat_names.extend(['inlier mean', 'upper bound of heavy range', 'upper fence', 'max (q4)', 'outliers'])

    vals = []

    for stats, level_act in zip(bxpstats, level_activity_counts):
        level_stats = [numpy.min(level_act), '%.2f' % stats['whislo'], stats['q1'], stats['med']]
        if notches:
            level_stats.append((stats['cilo'], stats['cihi']))
        n_outliers = len(stats['fliers'])
        level_stats.extend(['%.2f' % stats['mean'], stats['q3'], '%.2f' % stats['whishi'], numpy.max(level_act),
                            '{0:,}'.format(n_outliers) + ' (%.2f%%)' % (n_outliers * 100. / len(level_act))])

        vals.append(level_stats)

    vals = [stat_names] + vals

    fig = go.Figure(data=[go.Table(header=dict(values=['Statistic'] + [stats['label'] for stats in bxpstats],
                                               fill=dict(color='#AAAAAA')),
                                   cells=dict(values=vals))],
                    layout=go.Layout(
                        height=600,
                        title='Box Plot Statistics')
                    )

    plot(fig, 'level_boxplot_stats')

    # Chi squared tests for pairs of adjacent engagement levels
    indep_strs = []
    for level1, level2 in indep_level_pairs:
        indep_strs.append('it is at least ' + str(confidence * 100) + '% certain that users with ' + level1
                          + ' and ' + level2 + ' engagement levels have significantly different activity patterns')

    plot(go.Figure(data=[go.Table(header=dict(values=['Heterogenous activity distributions']),
                                  cells=dict(values=[indep_strs]))],
                   layout=go.Layout(width=800, autosize=False,
                                    title='Activity Independence of Adjacent Engagement Levels')),
         'indep')

    return plot_divs


def divs_to_file(divs, html_path, file_per_div=False, div_order=None):
    plotlyjs = '<script type="text/javascript">\n' + get_plotlyjs() + '\n</script>'

    if os.path.exists(html_path):
        assert file_per_div == os.path.isdir(html_path)
        if file_per_div:
            # delete all previous files
            for path in os.listdir(html_path):
                path = os.path.join(html_path, path)
                if os.path.isfile(path):
                    os.remove(path)
                elif os.path.isdir(path):
                    # we could also remove dirs with shutil.rmtree
                    pass
    elif file_per_div:
        os.makedirs(html_path)

    if file_per_div:
        prefix_template = '%0' + str(len(str(len(divs)))) + 'd'
        filename = os.path.join(html_path, os.extsep.join((prefix_template % 0 + '_plotlyjs', 'html')))
        with open(filename, 'wt') as div_file:
            div_file.write(plotlyjs)
    else:
        html_str = plotlyjs

    if div_order is None:
        div_order = divs.keys()
    else:
        assert set(div_order) <= set(divs.keys())

    for div_idx, div_name in enumerate(div_order):
        if file_per_div:
            filename = os.path.join(html_path,
                                    os.extsep.join((prefix_template % (div_idx + 1) + '_' + div_name, 'html')))
            with open(filename, 'wt') as div_file:
                div_file.write(divs[div_name])
        else:
            html_str += '\n' + divs[div_name]

    if not file_per_div:
        with open(html_path, 'wt') as html_plots_file:
            html_plots_file.write(html_str)

        webbrowser.open('file://' + html_path)


def main():
    # Data location and availability info

    # engagement data between 2017-02-01 and 2018-04-30 (151 days) for 23757 users
    points_filename = 'data/innertrends_169.logbook_engagement_points.csv'

    # activity data between 2012-11-01 and 2018-06-13 for 32244 users (activity data ends 45 days after engagement data)
    activity_filename = 'data/innertrends_169_activity.csv'

    # time after which we don't have any more data (e.g., database query time)
    activity_end_time = numpy.datetime64('2018-06-13', 'D')

    # caching is not used, but we could use it, later
    # points_cache_filename = num_uids_cache_filename = activity_cache_filename = str_uids_cache_filename = None

    # User params

    # we choose to read timestamps in the CSVs in the same time unit that will be used for computing scores to avoid
    # conversions
    time_unit = 'D'  # 'D' for days
    score_window = numpy.timedelta64(30, 'D')
    churn_time = numpy.timedelta64(30, 'D')

    # thresholds between user engagement classes; intervals are open on the left and closed on the right
    engagement_levels_score_thresholds = [0, 4, 418.93]
    engagement_levels = ['No', 'Low', 'Medium', 'High']

    # params for activity frequency analysis
    activity_window = numpy.timedelta64(30, 'D')

    # activity frequency is number of days in which a user scored engagement points, as opposed to number of days in
    # which _any_ activity was recorded (e.g., just logging in)
    active_means_engaged = True

    # analyze breaks instead of frequency over 30 days
    breaks = False

    # the desired confidence (significance) level for the chi-squared tests applied
    # pairwise on the activity histograms of adjacent engagement levels
    confidence = .95

    # when drawing the custom box plots, what proportion of the samples should be enclosed by the box's bounds
    # e.g., .95 = 95%
    min_range_weight = .95

    level_colors = ['purple', 'red', 'green', 'blue']

    # whether or not to compute confidence intervals for medians of per-level activity distributions; these can be used
    # to argue that distributions are different; however, it takes quite long to compute them.
    med_ci = True

    # when using plotly we get the most interactive graphs, but huge output files
    short_html = True

    # save divs in separate html files
    file_per_div = False

    # saves all plots in a single html file
    if file_per_div:
        html_plots_path = 'output/pieces'
    else:
        html_plots_path = ('output/plots'
                           + ('_short' if short_html else '_long')
                           + ('_eng' if active_means_engaged else '')
                           + ('' if file_per_div else '.html'))

    # print html size info to console
    debug_html_size = False

    # print info about the number of users, days, windows, etc. to console
    print_stats = True

    if short_html:
        plot_order = [
            # plots for all users
            'hist',   # more interactive than hist_kde
            'hist_kde',
            'violin',
            'box',
            'boxplot_stats',

            # plots by engagement level
            'hists',
            'violins',
            'boxes',
            'custom_boxes',
            'level_boxplot_stats',

            # independence
            'hists_overlaid_normed',
            'hists_overlaid_interactive_normed',
            'kdes_overlaid',
            'indep',

            # thresholds
            'hists_overlaid',
            'hists_overlaid_interactive',
            'thresholds',
        ]
    else:
        plot_order = [
            # plots for all users
            'hist',  # made by plotly, more interactive than hist_kde
            'hist_kde',
            'violin',
            'box',
            'boxplot_stats',

            # plots by engagement level
            'hists',
            'violins',
            'boxes',
            'custom_boxes',
            'level_boxplot_stats',

            # independence
            # 'split violins',
            # 'hists_overlaid_normed',   # we don't show this because the interactive plot by plotly looks good enough
            'hists_overlaid_interactive_normed',
            'kdes_overlaid',
            'indep',

            # thresholds
            # 'hists_overlaid',  # we don't show this because the interactive plot by plotly looks good enough
            'hists_overlaid_interactive',
            'thresholds',
        ]

    # check user params
    assert len(level_colors) == len(engagement_levels)
    assert len(engagement_levels) == len(engagement_levels_score_thresholds) + 1
    assert breaks == (activity_window is None), 'activity window size does not makes sense when analyzing breaks'

    # preproc user params
    if breaks:
        activity_window = None

    # load and preproc data
    activity, level_activity = get_activity(points_filename, activity_filename, time_unit, churn_time, activity_window,
                                            score_window, activity_end_time, engagement_levels_score_thresholds,
                                            active_means_engaged, print_stats)

    # convert to histograms
    activity_hist, level_activity_hists, activity_vals, activity_bins = get_activity_hists(activity, level_activity)

    # compute activity limits for each level
    level_activity_limits = get_level_activity_limits(activity_hist, level_activity_hists, activity_vals[0])

    # run independence tests on pairs of histograms of adjacent engagement levels
    indep_level_pairs = []
    for level1, level2, level1_activity_hist, level2_activity_hist \
            in zip(engagement_levels[:-1], engagement_levels[1:], level_activity_hists[:-1], level_activity_hists[1:]):
        if hists_independent(level1_activity_hist, level2_activity_hist):
            indep_level_pairs.append((level1, level2))

    if active_means_engaged:
        # don't plot stuff for users that have no engagement, it's too boring
        if engagement_levels[0] == indep_level_pairs[0][0]:
            indep_level_pairs = indep_level_pairs[1:]
        level_activity = level_activity[1:]
        level_activity_hists = level_activity_hists[1:]
        engagement_levels = engagement_levels[1:]
        level_colors = level_colors[1:]
        level_activity_limits = level_activity_limits[1:]

    # robust box plot statistics for all data (min_range_weight=None to keep standard quartiles)
    bxpstats_all = custom_bxpstats(activity, 'All', min_range_weight=None)

    # custom box plot data
    bxpstats = [custom_bxpstats(level_act, engagement_level, min_range_weight, med_ci)
                for level_act, engagement_level in zip(level_activity, engagement_levels)]

    # plot data
    plot_divs = get_plot_divs(activity, level_activity, activity_vals, activity_hist, level_activity_hists,
                              activity_bins, activity_window, engagement_levels, level_colors, level_activity_limits,
                              bxpstats_all, bxpstats, indep_level_pairs, confidence, short_html, debug_html_size)

    # write data to disk and open browser
    divs_to_file(plot_divs, html_plots_path, file_per_div, plot_order)


if __name__ == '__main__':
    main()
